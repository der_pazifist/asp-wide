package it.unical.mat.aspwide.testing.model;

import java.util.ArrayList;

public class TestSuiteResult extends TestSuite {

	private ArrayList<TestResult> testResults;
	private ArrayList<TestError> testErrors;
	
	public TestSuiteResult() {
		super();
		this.testResults = new ArrayList<TestResult>();
		this.testErrors = new ArrayList<TestError>();
	}
	
	public TestSuiteResult(TestSuite ts) {
		super(ts);
		this.testResults = new ArrayList<TestResult>();
	}
	
	public void addTestResult(TestResult tr) {
		this.testResults.add(tr);
	}
	
	public void addTestError(TestError err) {
		this.testErrors.add(err);
	}

	public ArrayList<TestResult> getTestResults() {
		return testResults;
	}

	public void setTestResults(ArrayList<TestResult> testResults) {
		this.testResults = testResults;
	}

}
