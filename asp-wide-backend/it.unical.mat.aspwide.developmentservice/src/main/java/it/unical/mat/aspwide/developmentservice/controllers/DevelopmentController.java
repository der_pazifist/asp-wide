package it.unical.mat.aspwide.developmentservice.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.unical.mat.aspwide.developmentservice.logic.DevelopmentLogic;
import it.unical.mat.aspwide.model.developmentservice.TestResults;
import it.unical.mat.aspwide.model.developmentservice.TestableFile;
import it.unical.mat.aspwide.model.developmentservice.ValidatableFile;
import it.unical.mat.aspwide.model.developmentservice.ValidationResult;

@RestController
public class DevelopmentController {
	
	private final DevelopmentLogic logic;

	public DevelopmentController(DevelopmentLogic logic) {
		this.logic = logic;
	}
	
	@PostMapping(value = "/validate")
	public ResponseEntity<ValidationResult> validate(@RequestBody ValidatableFile req){
		ValidationResult resp = this.logic.validateCode(req);
		
		return ResponseEntity.ok(resp);
	}
	
	@PostMapping(value = "/test")
	public ResponseEntity<TestResults> test(@RequestBody TestableFile req){
		TestResults resp = this.logic.testCode(req);
		
		return ResponseEntity.ok(resp);
	}
	
	
}
