package it.unical.mat.aspwide.testing.model;

public class AssertionResult {
	
	private String executedCode;
	private String executionOutput;
	private boolean succeeded;
	
	public AssertionResult() {
	}

	public String getExecutedCode() {
		return executedCode;
	}

	public void setExecutedCode(String executedCode) {
		this.executedCode = executedCode;
	}

	public String getExecutionOutput() {
		return executionOutput;
	}

	public void setExecutionOutput(String executionOutput) {
		this.executionOutput = executionOutput;
	}

	public boolean isSucceeded() {
		return succeeded;
	}

	public void setSucceeded(boolean succeeded) {
		this.succeeded = succeeded;
	}

}
