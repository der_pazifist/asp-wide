package it.unical.mat.aspwide.testing.model;

public class TestError {

	private String name;
	private String description;
	
	public TestError() {
		// TODO Auto-generated constructor stub
	}

	public TestError(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
