package it.unical.mat.aspwide.developmentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevelopmentService {

	public static void main(String[] args) {
		SpringApplication.run(DevelopmentService.class, args);
	}
}
