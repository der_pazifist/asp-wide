package it.unical.mat.aspwide.parser.aspcore2.annotations;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

@Component
public class AspCore2wAnnotationsParserVisitorImpl extends AspCore2wAnnotationsAbstractParserVisitor {

	private ArrayList<AC2wA_Atom> allAtoms;
	private ArrayList<AC2wA_Atom> bodyAtoms;
	private ArrayList<AC2wA_Atom> headAtoms;
	private ArrayList<AC2wA_VariableTerm> posVars;
	private ArrayList<AC2wA_VariableTerm> negVars;

	public AspCore2wAnnotationsParserVisitorImpl() {
		super();
		this.allAtoms = new ArrayList<AC2wA_Atom>();
		this.bodyAtoms = new ArrayList<AC2wA_Atom>();
		this.headAtoms = new ArrayList<AC2wA_Atom>();
		this.posVars = new ArrayList<AC2wA_VariableTerm>();
		this.negVars = new ArrayList<AC2wA_VariableTerm>();
	}

	@SuppressWarnings("unchecked")
	public AspCore2wAnnotationsParserVisitorImpl(AspCore2wAnnotationsParserVisitorImpl visitor) {
		super();
		this.allAtoms = (ArrayList<AC2wA_Atom>) visitor.getAllAtoms().clone();
		this.bodyAtoms = (ArrayList<AC2wA_Atom>) visitor.getBodyAtoms().clone();
		this.headAtoms = (ArrayList<AC2wA_Atom>) visitor.getHeadAtoms().clone();
		this.posVars = (ArrayList<AC2wA_VariableTerm>) visitor.getPosVars().clone();
		this.negVars = (ArrayList<AC2wA_VariableTerm>) visitor.getNegVars().clone();
	}

	public void addAtom(AC2wA_Atom atom) {
		
		if(!allAtoms.contains(atom))
			allAtoms.add(atom);
		
		// determine whether the current atom is in head or body of a rule
		boolean isHeadAtom = false;
		Node currNode = atom.jjtGetParent();
		
		// as long as the parent is not a conjunction (indicates body), we continue the search for a disjunction (indicates head)
		while(!currNode.getClass().getName().equals(AC2wA_Body.class.getName())) {
			if(currNode.getClass().getName().equals(AC2wA_Disjunction.class.getName())){
				isHeadAtom = true;
				break; // leave loop as determined that this is head
			}
			else if(currNode.jjtGetParent() != null) {
				currNode = currNode.jjtGetParent();
			}
			else
				break; // leave loop as we are at the root node
		}
		
		if(isHeadAtom) {
			if(!this.headAtoms.contains(atom))
				this.headAtoms.add(atom);
		}
		else {
			if(!this.bodyAtoms.contains(atom))
				this.bodyAtoms.add(atom);
		}
    }
	
	public void addVariable(AC2wA_VariableTerm var) {
		boolean isPosVar = true;
		Node currNode = var.jjtGetParent();
		
		// until the parent is not a conjunction it could be negative
		while(!currNode.getClass().getName().equals(AC2wA_Body.class.getName())) {
			// if parent is an aggregate we can leave the loop
			if(currNode.getClass().getName().equals(AC2wA_AggregateElement.class.getName())) {
				break;
			}
			else if(currNode.getClass().getName().equals(AC2wA_NafClassicalLiteral.class.getName())
					|| currNode.getClass().getName().equals(AC2wA_NafAggregate.class.getName())
					|| currNode.getClass().getName().equals(AC2wA_Disjunction.class.getName())
					|| currNode.getClass().getName().equals(AC2wA_NegativeBuiltinAtom.class.getName())
					|| currNode.getClass().getName().equals(AC2wA_BasicTerms.class.getName())
					|| currNode.getClass().getName().equals(AC2wA_WeakTerm.class.getName())) {
				isPosVar = false;
				break;
			}
			else if(currNode.jjtGetParent() != null) {
				currNode = currNode.jjtGetParent();
			}
			else {
				break;
			}
		}
		
		if(isPosVar) {
			if(!this.posVars.contains(var))
				this.posVars.add(var);
		}
		else {
			if(!this.negVars.contains(var))
				this.negVars.add(var);
		}
	}
	
	private void clearAtoms() {
		this.allAtoms.clear();
		this.bodyAtoms.clear();
		this.headAtoms.clear();
		this.posVars.clear();
		this.negVars.clear();
	}

	@Override
	public void resetVisitor() {
		this.clearAtoms();
	}

	@Override
	public AspCore2wAnnotationsParserVisitorImpl clone() {
		return new AspCore2wAnnotationsParserVisitorImpl(this);
	}

	public ArrayList<AC2wA_Atom> getAllAtoms() {
		return allAtoms;
	}

	public ArrayList<AC2wA_Atom> getBodyAtoms() {
		return bodyAtoms;
	}

	public ArrayList<AC2wA_Atom> getHeadAtoms() {
		return headAtoms;
	}

	public ArrayList<AC2wA_VariableTerm> getPosVars() {
		return posVars;
	}

	public ArrayList<AC2wA_VariableTerm> getNegVars() {
		return negVars;
	}
}
