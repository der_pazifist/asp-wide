package it.unical.mat.aspwide.developmentservice.logic;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;


import it.unical.mat.aspwide.developmentservice.utils.ValidationUtils;
import it.unical.mat.aspwide.model.developmentservice.CodeError;
import it.unical.mat.aspwide.model.developmentservice.CodeWarning;
import it.unical.mat.aspwide.model.developmentservice.TestResults;
import it.unical.mat.aspwide.model.developmentservice.TestableFile;
import it.unical.mat.aspwide.model.developmentservice.ValidatableFile;
import it.unical.mat.aspwide.model.developmentservice.ValidationResult;
import it.unical.mat.aspwide.parser.aspcore2.annotations.AC2wA_Atom;
import it.unical.mat.aspwide.parser.aspcore2.annotations.AC2wA_Program;
import it.unical.mat.aspwide.parser.aspcore2.annotations.AC2wA_VariableTerm;
import it.unical.mat.aspwide.parser.aspcore2.annotations.AspCore2wAnnotationsAbstractParserVisitor;
import it.unical.mat.aspwide.parser.aspcore2.annotations.AspCore2wAnnotationsParser;
import it.unical.mat.aspwide.parser.aspcore2.annotations.AspCore2wAnnotationsParserVisitorImpl;
import it.unical.mat.aspwide.parser.aspcore2.annotations.ParseException;
import it.unical.mat.aspwide.parser.aspcore2.annotations.TokenMgrError;
import it.unical.mat.aspwide.testing.RuleCrawler;
import it.unical.mat.aspwide.testing.model.TestSuite;

@Component
public class DevelopmentLogic {
	
	private AspCore2wAnnotationsAbstractParserVisitor parserVisitor;

	public DevelopmentLogic(AspCore2wAnnotationsAbstractParserVisitor visitor) {
		this.parserVisitor = visitor;
	}
	
	public TestResults testCode(TestableFile request) {
		if(request.getContent() == null)
			return null;
		
		TestSuite ts = new TestSuite();
		
		// add rules (and blocks) to test suite
		RuleCrawler rc = new RuleCrawler(ts);
		rc.crawlForRules(request.getContent());
		
		try {
			AspCore2wAnnotationsParser parser = new AspCore2wAnnotationsParser(
					new ByteArrayInputStream(request.getContent().getBytes()));
			parser.configureTestSuite(ts);
			parser.program();
		}
		catch(ParseException ex) {
			ex.printStackTrace();
		}
		catch(TokenMgrError err) {
			err.printStackTrace();
		}

		return new TestResults();
	}
	
	public ValidationResult validateCode(ValidatableFile request) {
		ValidationResult response = new ValidationResult(request);
		
		// check for errors
		response.setErrors(this.checkCodeValidity(request.getContent()));
		response.getErrors().addAll(this.checkSemanticErrors(request.getContent()));
		
		// check for warnings
		if(response.isExecutable())
			response.setWarnings(this.checkWarnings(request.getContent()));
		
		return response;
	}
	
	private List<CodeError> checkCodeValidity(String code) {
		List<CodeError> errors = new ArrayList<CodeError>();
		
		if(code == null) {
			CodeError ce = new CodeError();
			ce.name = "ERROR: no code specified for validation.";
			ce.description = "ERROR: no code specified for validation.";
			errors.add(ce);
			
			return errors;
		}
		
		try {
			AspCore2wAnnotationsParser parser = new AspCore2wAnnotationsParser(
					new ByteArrayInputStream( code.getBytes() ));
			parser.program();
		}
		catch(ParseException ex) {
			ex.printStackTrace();

			CodeError ce = new CodeError();
			ce.line = ex.currentToken.beginLine;
			// correct beginColumn by -2 as the parser has one symbol lookahead and is 2 position off
			ce.startIndex = ex.currentToken.next.beginColumn;
			ce.endIndex = ex.currentToken.next.endColumn;
			
			String tmpErrMsg = ex.getMessage();
			tmpErrMsg = tmpErrMsg.replaceFirst("line [0-9]+", "line "+ex.currentToken.beginLine);
			tmpErrMsg = tmpErrMsg.replaceFirst("column [0-9]+", "column "+ce.startIndex);
			ce.name = tmpErrMsg;
			
			ce.description = ex.getStackTrace().toString();
			errors.add(ce);
		}
		catch(TokenMgrError err) {
			err.printStackTrace();
		}
		
		return errors;

	}
	
	private List<CodeError> checkSemanticErrors(String code) {
		List<CodeError> errors = new ArrayList<CodeError>();
		
		if(code == null) {
			CodeError ce = new CodeError();
			ce.name = "ERROR: no code specified for semantic error checking.";
			ce.description = "ERROR: no code specified for semantic error checking.";
			errors.add(ce);
			
			return errors;
		}

		String codeWithoutComments = this.removeComments(code);
		
		// extract the rules of the program
		List<String> rules = new ArrayList<String>();
		Pattern rulePattern = Pattern.compile(ValidationUtils.getInstance().ruleRegex);
		Matcher matcher = rulePattern.matcher(codeWithoutComments);
		while(matcher.find()) {
			rules.add(matcher.group());
		}
		
		// check the rules of the program
		int currLine = 1;
		int currPos = 0;
		for(String rule: rules) {
			
			// update rule position
			int countNewLines = rule.split("\\n").length-1;
			if(countNewLines > 0){
				currLine += countNewLines;
				currPos = 0;
			}
						
			try {
				this.parserVisitor.resetVisitor();
				AC2wA_Program program = ValidationUtils.getInstance().createParserProgram(rule);
				program.jjtAccept(this.parserVisitor, null);
				
				// check for rule safety
				for(AC2wA_VariableTerm var: ((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getNegVars()) {
					if(!((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getPosVars().contains(var)) {
						
						CodeError ce = new CodeError();
						ce.line = currLine;
						ce.startIndex = currPos;
						ce.endIndex = currPos + rule.length();
						
						ce.name = "ERROR: Rule safety violation at line "+currLine+". The variable '"+var.jjtGetValue().toString()+"' does not appear in any positive atom.";
						ce.description = "ERROR: Rule safety violation at line "+currLine+". The variable '"+var.jjtGetValue().toString()+"' does not appear in any positive atom.";
						errors.add(ce);
					}
				}
				
			}
			catch(ParseException ex) {
				ex.printStackTrace();

				// These errors are already tracked by function checkCodeValidity()
				
				CodeError ce = new CodeError();
				ce.line = currLine;
				// correct beginColumn by -2 as the parser has one symbol lookahead and is 2 position off
				ce.startIndex = currPos  + ex.currentToken.next.beginColumn-2;
				ce.endIndex = currPos  + ex.currentToken.next.endColumn;
				
				String tmpErrMsg = ex.getMessage();
				tmpErrMsg = tmpErrMsg.replaceFirst("line [0-9]+", "line "+currLine);
				tmpErrMsg = tmpErrMsg.replaceFirst("column [0-9]+", "column "+ce.startIndex);
				ce.name = tmpErrMsg;
				
				ce.description = ex.getStackTrace().toString();
				errors.add(ce);
			}
			catch(TokenMgrError err) {
				err.printStackTrace();
			}

			currPos += rule.length();
		}
		
		return errors;

	}
	
	// removes comments but keeps newlines
	private String removeComments(String code) {
		String newCode = code;
		Pattern rulePattern = Pattern.compile(ValidationUtils.getInstance().commentRegex);
		Matcher matcher = rulePattern.matcher(code);
		
		while(matcher.find()) {
			String match = matcher.group();
			String replacementString = "";
			int newlineCount = match.split("\n").length;
			
			for(int i = 0; i < newlineCount-1; i++)
				replacementString = replacementString + "\n";
			
			newCode = newCode.replace(match, replacementString);
		}
		
		return newCode;
	}
	
	@SuppressWarnings("unchecked")
	private List<CodeWarning> checkWarnings(String code) {
		List<CodeWarning> warnings = new ArrayList<CodeWarning>();

		if(code == null) {
			CodeWarning cw = new CodeWarning();
			cw.name = "WARNING: no code specified for checking warnings.";
			cw.description = "WARNING: no code specified for checking warnings.";
			warnings.add(cw);
			
			return warnings;
		}
		
		String codeWithoutComments = this.removeComments(code);
		
		// extract the rules of the program
		List<String> rules = new ArrayList<String>();
		Pattern rulePattern = Pattern.compile(ValidationUtils.getInstance().ruleRegex);
		Matcher matcher = rulePattern.matcher(codeWithoutComments);
		while(matcher.find()) {
			rules.add(matcher.group());
		}
		
		this.parserVisitor.resetVisitor();
		ArrayList<AC2wA_Atom> allAtoms = new ArrayList<AC2wA_Atom>();
		ArrayList<AC2wA_Atom> headAtoms = new ArrayList<AC2wA_Atom>();
//		ArrayList<ASPCore2Atom> bodyAtoms = new ArrayList<ASPCore2Atom>();
		
		
		// get all the atoms from visitor for later
		try {
			AC2wA_Program program = ValidationUtils.getInstance().createParserProgram(code);
			program.jjtAccept(this.parserVisitor, null);
			
			// copy for later
			allAtoms = (ArrayList<AC2wA_Atom>) ((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getAllAtoms().clone();
			headAtoms = (ArrayList<AC2wA_Atom>) ((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getHeadAtoms().clone();
//			bodyAtoms = (ArrayList<ASPCore2Atom>) ((ASPCore2ParserVisitorImpl) this.parserVisitor).getBodyAtoms().clone();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		// check the rules of the program
		int currLine = 1;
		for(String rule: rules) {
			
			// update rule position
			int countNewLines = rule.split("\\n").length-1;
			if(countNewLines > 0){
				currLine += countNewLines;
			}
						
			try {
				this.parserVisitor.resetVisitor();
				AC2wA_Program program = ValidationUtils.getInstance().createParserProgram(rule);
				program.jjtAccept(this.parserVisitor, null);
				
				// check for warning: all atom should appear in a head
				for(AC2wA_Atom atom: ((AspCore2wAnnotationsParserVisitorImpl)this.parserVisitor).getBodyAtoms()){
					// check if the current atom occurs in any head
					if(!headAtoms.stream().anyMatch((currAtom -> currAtom.equalsName(atom)))) {
						CodeWarning cw = new CodeWarning();
						cw.line = currLine;
						cw.name = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' should also appear in the head of a rule. Line "+currLine+".";
						cw.description = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' should also appear in the head of a rule."+currLine+".";

						warnings.add(cw);
					}
				}
				
				// check for warning: atoms with different arity
				for(AC2wA_Atom atom: ((AspCore2wAnnotationsParserVisitorImpl)this.parserVisitor).getAllAtoms()){
					// check if the current atom occurs with a different arity
					if(allAtoms.stream().anyMatch(currAtom -> (currAtom.equalsName(atom) && currAtom.getArity() != atom.getArity()))) {
						CodeWarning cw = new CodeWarning();
						cw.line = currLine;
						cw.name = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' was already defined with a different arity. Line "+currLine+".";
						cw.description = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' was already defined with a different arity. Line "+currLine+".";

						warnings.add(cw);
					}
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		return warnings;
	}
	
}
