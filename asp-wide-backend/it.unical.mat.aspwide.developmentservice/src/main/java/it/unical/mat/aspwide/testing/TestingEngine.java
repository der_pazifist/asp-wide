package it.unical.mat.aspwide.testing;


import it.unical.mat.aspwide.testing.model.*;

public class TestingEngine {

	public TestingEngine() {
	}
	
	public TestSuiteResult executeTestSuite(TestSuite ts) {
		TestSuiteResult tsr = new TestSuiteResult(ts);
		
		for(Test test: ts.getTests()) {
			try {
				TestResult tr = this.executeTest(test, ts);
				tsr.addTestResult(tr);
			} catch(Exception e) {
				tsr.addTestError(
						new TestError(
								e.getMessage(),
								e.getStackTrace().toString()));
			}
		}
		

		return tsr;
	}
	
	public TestResult executeTest(Test test, TestSuite ts) throws Exception {
		TestResult testResult = new TestResult(test);
		
		for(Assertion as: test.getAssertions()) {
			AssertionResult ar = this.executeAssertion(as, test, ts);
			testResult.addAssertionResult(ar);
		}
		
		return testResult;
	}
	
	public AssertionResult executeAssertion(Assertion as, Test test, TestSuite ts) throws Exception {
		AssertionResult ar = null;
		String executedCode = "";
		
		// specify input
		if(test.getInput() != null) {
			executedCode = executedCode + test.getInput();
		}
		
		// specify scope
		for(String scopeName: test.getScopes()) {
			if(ts.containsBlock(scopeName)) {
				executedCode += ts.getBlock(scopeName);
			}
			else if (ts.containsRule(scopeName)) {
				executedCode += ts.getRule(scopeName);
			}
			else {
				throw new Exception("ERROR: Scope with name '"+scopeName+"' in test '"+test.getName()+"' cannot be found.");
			}
		}
		
		// check the right assertion
		if(as instanceof AssertTrueInAll) {
			ar = checkAssertion(executedCode, (AssertTrueInAll)as);
		}
		else if(as instanceof AssertTrueInAtLeast) {
			ar = checkAssertion(executedCode, (AssertTrueInAtLeast)as);
		}
		else if(as instanceof AssertTrueInAtMost) {
			ar = checkAssertion(executedCode, (AssertTrueInAtMost)as);
		}
		else if(as instanceof AssertTrueInExactly) {
			ar = checkAssertion(executedCode, (AssertTrueInExactly)as);
		}
		else if(as instanceof AssertFalseInAll) {
			ar = checkAssertion(executedCode, (AssertFalseInAll)as);
		}
		else if(as instanceof AssertFalseInAtLeast) {
			ar = checkAssertion(executedCode, (AssertFalseInAtLeast)as);
		}
		else if(as instanceof AssertFalseInAtMost) {
			ar = checkAssertion(executedCode, (AssertFalseInAtMost)as);
		}
		else if(as instanceof AssertFalseInExactly) {
			ar = checkAssertion(executedCode, (AssertFalseInExactly)as);
		}
		else if(as instanceof AssertConstraint) {
			ar = checkAssertion(executedCode, (AssertConstraint)as);
		}
		else if(as instanceof AssertConstraintInAtLeast) {
			ar = checkAssertion(executedCode, (AssertConstraintInAtLeast)as);
		}
		else if(as instanceof AssertConstraintInAtMost) {
			ar = checkAssertion(executedCode, (AssertConstraintInAtMost)as);
		}
		else if(as instanceof AssertConstraintInExactly) {
			ar = checkAssertion(executedCode, (AssertConstraintInExactly)as);
		}
		else if(as instanceof AssertBestModelCost) {
			ar = checkAssertion(executedCode, (AssertBestModelCost)as);
		}

		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertTrueInAll as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertTrueInAtLeast as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertTrueInAtMost as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertTrueInExactly as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertFalseInAll as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertFalseInAtLeast as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertFalseInAtMost as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertFalseInExactly as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertConstraint as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertConstraintInAtLeast as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertConstraintInAtMost as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertConstraintInExactly as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
	public AssertionResult checkAssertion(String code, AssertBestModelCost as) {
		AssertionResult ar = new AssertionResult();
		ar.setExecutedCode(code);
		
		return ar;
	}
	
}
