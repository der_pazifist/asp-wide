package it.unical.mat.aspwide.testing.model;

import java.util.ArrayList;

public class Test {

	private String name;
	private String input;
	private String inputFile;
	private ArrayList<String> scopes;
	private ArrayList<Assertion> assertions;

	public Test() {
		super();
		this.scopes = new ArrayList<String>();
		this.assertions = new ArrayList<Assertion>();
	}
	
	public Test(Test t) {
		super();
		this.name = t.getName();
		this.input = t.getInput();
		this.inputFile = t.getInputFile();
		this.scopes = new ArrayList<String>(t.getScopes());
		this.assertions = new ArrayList<Assertion>(t.getAssertions());
	}
		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInput() {
		return input;
	}
	
	public void setInput(String input) {
		this.input = input;
	}
	
	public String getInputFile() {
		return inputFile;
	}
	
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	
	public ArrayList<String> getScopes() {
		return scopes;
	}
	
	public void setScopes(ArrayList<String> scopes) {
		this.scopes = scopes;
	}
	
	public ArrayList<Assertion> getAssertions() {
		return assertions;
	}
	
	public void setAssertions(ArrayList<Assertion> assertions) {
		this.assertions = assertions;
	}
	
	public void addScope(String scope) {
		this.scopes.add(scope);
	}
	
	public void addAssertion(Assertion assertion) {
		this.assertions.add(assertion);
	}

}
