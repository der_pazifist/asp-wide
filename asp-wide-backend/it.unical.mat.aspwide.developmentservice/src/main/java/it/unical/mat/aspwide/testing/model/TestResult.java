package it.unical.mat.aspwide.testing.model;

import java.util.ArrayList;

public class TestResult extends Test {

	private ArrayList<AssertionResult> assertionResults;
	
	public TestResult() {
		super();
		this.assertionResults = new ArrayList<AssertionResult>();
	}
	
	public TestResult(Test test) {
		super(test);
		this.assertionResults = new ArrayList<AssertionResult>();
	}
	
	public boolean testSucceeded() {
		if(this.assertionResults!= null) {
			boolean succeeded = true;
			for(AssertionResult ar: this.assertionResults) {
				if(!ar.isSucceeded()) {
					succeeded = false;
					break;
				}
			}
			return succeeded;
		}
		
		return false;
	}
	
	public void addAssertionResult(AssertionResult ar) {
		this.assertionResults.add(ar);
	}

	public ArrayList<AssertionResult> getAssertionResults() {
		return assertionResults;
	}

	public void setAssertionResults(ArrayList<AssertionResult> assertionResults) {
		this.assertionResults = assertionResults;
	}

}
