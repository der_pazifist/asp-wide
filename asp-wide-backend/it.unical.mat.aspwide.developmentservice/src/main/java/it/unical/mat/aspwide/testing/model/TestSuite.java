package it.unical.mat.aspwide.testing.model;

import java.util.ArrayList;
import java.util.HashMap;

public class TestSuite {

	private HashMap<String, Block> blocks;
	private HashMap<String, String> rules;
	private ArrayList<Test> tests;
	
	public TestSuite() {
		this.blocks = new HashMap<String, Block>();
		this.rules = new HashMap<String, String>();
		this.tests = new ArrayList<Test>();
	}
	
	public TestSuite(TestSuite ts) {
		this.blocks = new HashMap<String, Block>(ts.getBlocks());
		this.rules = new HashMap<String, String>(ts.getRules());
		this.tests = new ArrayList<Test>(ts.getTests());
	}
	
	public void addOrUpdateRule(String name, String rule) {
		this.rules.put(name, rule);
	}
	
	public String getRule(String name) {
		return this.rules.get(name);
	}
	
	public boolean containsRule(String name) {
		return this.rules.containsKey(name);
	}
	
	public void removeRule(String name) {
		this.rules.remove(name);
	}
	
	public void clearRules() {
		this.rules.clear();
	}
	
	public void createBlock(String name) {
		if(!this.blocks.containsKey(name))
			this.addOrUpdateBlock(name, new Block());
	}
	
	private void addOrUpdateBlock(String name, Block block) {
		this.blocks.put(name, block);
	}
	
	public void addRuleAssignment(String blockName, String ruleName) {
		if(!this.blocks.containsKey(blockName))
			this.addOrUpdateBlock(blockName, new Block());
		
		this.blocks.get(blockName).addRuleAssignment(ruleName);
	}
	
	public void removeRuleAssignment(String blockName, String ruleName) {
		this.blocks.get(blockName).removeRuleAssignment(ruleName);
	}
	
	public Block getBlock(String name) {
		return this.blocks.get(name);
	}
	
	public boolean containsBlock(String name) {
		return this.blocks.containsKey(name);
	}
	
	public void removeBlock(String name) {
		this.blocks.remove(name);
	}
	
	public void clearBlocks() {
		this.blocks.clear();
	}

	public ArrayList<Test> getTests() {
		return tests;
	}

	public void setTests(ArrayList<Test> tests) {
		this.tests = tests;
	}
	
	public void addTest(Test test) {
		this.tests.add(test);
	}

	public HashMap<String, Block> getBlocks() {
		return blocks;
	}

	public HashMap<String, String> getRules() {
		return rules;
	}
	
	

}
