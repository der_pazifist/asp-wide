package it.unical.mat.aspwide.executionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExecutionService {

	public static void main(String[] args) {
		SpringApplication.run(ExecutionService.class, args);
	}
}