package it.unical.mat.aspwide.executionservice.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.unical.mat.aspwide.executionservice.logic.ExecutionLogic;
import it.unical.mat.aspwide.model.executionservice.ExecutableFile;
import it.unical.mat.aspwide.model.executionservice.ExecutionResult;

@RestController
public class ExecutionController {
	
	private final ExecutionLogic logic;

	public ExecutionController(ExecutionLogic logic) {
		this.logic = logic;
	}
	
	@PostMapping(value = "/execute")
	public ResponseEntity<ExecutionResult> validate(@RequestBody ExecutableFile req){
		ExecutionResult resp = this.logic.executeCode(req);
		
		return ResponseEntity.ok(resp);
	}
	
//	@PostMapping(value = "/executetest")
//	public ResponseEntity<ExecutionResult> executeTest(@RequestBody ExecutableFile req){
//		ExecutionResult resp = this.logic.executeTest(req);
//		
//		return ResponseEntity.ok(resp);
//	}
	
	
	
}
