package it.unical.mat.aspwide.model.executionservice;

import java.util.ArrayList;

import it.unical.mat.aspwide.model.developmentservice.ValidatableFile;
import it.unical.mat.aspwide.model.developmentservice.ValidationResult;

public class ExecutionResult extends ValidationResult {
	
	protected ArrayList<Model> models;
	protected String result;

	public ExecutionResult() {
		super();
	}

	public ExecutionResult(String result) {
		super();
		this.result = result;
	}

	public ExecutionResult(String filename, String content) {
		super(filename, content);
		// TODO Auto-generated constructor stub
	}

	public ExecutionResult(ValidatableFile file) {
		super(file);
		// TODO Auto-generated constructor stub
	}
	
	public ExecutionResult(ValidationResult result) {
		super(result);
	}
	
	public void addModel(Model m) {
		this.models.add(m);
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public ArrayList<Model> getModels() {
		return models;
	}

	public void setModels(ArrayList<Model> models) {
		this.models = models;
	}
}
