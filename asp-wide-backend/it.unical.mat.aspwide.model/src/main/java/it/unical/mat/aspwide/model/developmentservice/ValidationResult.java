package it.unical.mat.aspwide.model.developmentservice;

import java.util.List;

public class ValidationResult extends ValidatableFile {
	
	protected List<CodeError> errors;
	protected List<CodeWarning> warnings;

	public ValidationResult() {
		super();
	}

	public ValidationResult(String filename, String content) {
		super(filename, content);
		// TODO Auto-generated constructor stub
	}
	
	public ValidationResult(ValidatableFile file) {
		super(file.getFilename(), file.getContent());
		// TODO Auto-generated constructor stub
	}
	
	public ValidationResult(ValidationResult file) {
		super(file.getFilename(), file.getContent());
		this.setErrors(file.getErrors());
		this.setWarnings(file.getWarnings());
	}

	public boolean isExecutable() {
		return (this.errors != null) ? this.errors.size() <= 0 : false;
	}

	public List<CodeError> getErrors() {
		return errors;
	}

	public void setErrors(List<CodeError> errors) {
		this.errors = errors;
	}

	public List<CodeWarning> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<CodeWarning> warnings) {
		this.warnings = warnings;
	}

}
