package it.unical.mat.aspwide.model;

public class LogicFile extends AbstractLogicItem {
	
	protected String content;

	public LogicFile() {
		super();
	}

	public LogicFile(String name, String content) {
		super();
		this.name = name;
		this.content = content;
	}

	public LogicFile(String content) {
		super();
		this.content = content;
	}

	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getFilename() {
		return name;
	}
	
	public void setFilename(String filename) {
		this.name = filename;
	}
}
