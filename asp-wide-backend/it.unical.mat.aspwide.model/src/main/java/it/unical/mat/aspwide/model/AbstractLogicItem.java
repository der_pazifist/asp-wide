package it.unical.mat.aspwide.model;

public abstract class AbstractLogicItem {
	
	protected String name;
	protected String uuid;

	public AbstractLogicItem() {
		super();
	}

	public AbstractLogicItem(String uuid, String name) {
		super();
		this.name = name;
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
