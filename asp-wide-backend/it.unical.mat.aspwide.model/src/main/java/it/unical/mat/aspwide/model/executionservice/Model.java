package it.unical.mat.aspwide.model.executionservice;

import java.util.ArrayList;
import java.util.HashMap;

public class Model {
	
	private ArrayList<Atom> atoms;
	private HashMap<Integer, Integer> costsAtLevel;
	
	public Model() {
		this.atoms = new ArrayList<Atom>();
		this.costsAtLevel = new HashMap<Integer, Integer>();
	}
	
	public void addAtom(Atom atom) {
		this.atoms.add(atom);
	}
	
	public void addCostAtlevel(int cost, int level) {
		this.costsAtLevel.put(level, cost);
	}

	public ArrayList<Atom> getAtoms() {
		return atoms;
	}

	public void setAtoms(ArrayList<Atom> atoms) {
		this.atoms = atoms;
	}
}
