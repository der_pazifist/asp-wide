package it.unical.mat.aspwide.model.developmentservice;

import java.util.List;

public class TestResults extends TestableFile {
	
	protected List<CodeError> errors;
	protected List<CodeWarning> warnings;

	public TestResults() {
		super();
	}

	public TestResults(String filename, String content) {
		super(filename, content);
		// TODO Auto-generated constructor stub
	}
	
	public TestResults(ValidatableFile file) {
		super(file.getFilename(), file.getContent());
		// TODO Auto-generated constructor stub
	}
	
	public TestResults(TestResults file) {
		super(file.getFilename(), file.getContent());
		this.setErrors(file.getErrors());
		this.setWarnings(file.getWarnings());
	}

	public boolean isExecutable() {
		return (this.errors != null) ? this.errors.size() <= 0 : true;
	}

	public List<CodeError> getErrors() {
		return errors;
	}

	public void setErrors(List<CodeError> errors) {
		this.errors = errors;
	}

	public List<CodeWarning> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<CodeWarning> warnings) {
		this.warnings = warnings;
	}

}
