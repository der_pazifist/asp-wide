package it.unical.mat.aspwide.service.model.files;

import java.util.List;

public class LogicFolder extends AbstractLogicItem{
	
	protected List<LogicFolder> folders;
	protected List<LogicFile> files;

	public LogicFolder() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LogicFolder(List<LogicFolder> folders, List<LogicFile> files) {
		super();
		this.folders = folders;
		this.files = files;
	}

	public LogicFolder(String uuid, String name, String path, List<LogicFolder> folders, List<LogicFile> files) {
		super(uuid, name, path);
		this.folders = folders;
		this.files = files;
	}

	public LogicFolder(String name, String uuid, String path) {
		super(name, uuid, path);
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<LogicFolder> getFolders() {
		return folders;
	}


	public void setFolders(List<LogicFolder> folders) {
		this.folders = folders;
	}


	public List<LogicFile> getFiles() {
		return files;
	}


	public void setFiles(List<LogicFile> files) {
		this.files = files;
	}

}
