package it.unical.mat.aspwide.service.model.testing;

public abstract class AtomAssertion extends Assertion {

	private String atoms;
	private int assertCount;
	
	public String getAtoms() {
		return atoms;
	}
	public void setAtoms(String atoms) {
		this.atoms = atoms;
	}
	public int getAssertCount() {
		return assertCount;
	}
	public void setAssertCount(int assertCount) {
		this.assertCount = assertCount;
	}
	
}
