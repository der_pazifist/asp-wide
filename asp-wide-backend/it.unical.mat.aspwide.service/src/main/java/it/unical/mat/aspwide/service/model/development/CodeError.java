package it.unical.mat.aspwide.service.model.development;

public class CodeError {
	
	public String name;
	public String description;
	public int line;
	public int startIndex;
	public int endIndex;
	
	public CodeError() {
	}

	public CodeError(String name, String description, int line) {
		super();
		this.name = name;
		this.description = description;
		this.line = line;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	
	
}
