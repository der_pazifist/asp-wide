package it.unical.mat.aspwide.service.model.testing;

import java.util.ArrayList;

public class Block {
	
	// stores the names of the assigned rules
	private ArrayList<String> assignedRules;
	
	public Block() {
		this.assignedRules = new ArrayList<String>();
	}
	
	public void addRuleAssignment(String name) {
		if(!this.assignedRules.contains(name))
			this.assignedRules.add(name);
	}
	
	public void removeRuleAssignment(String name) {
		this.assignedRules.remove(name);
	}
	
	public void clearRuleAssignments() {
		this.assignedRules.clear();
	}

	public ArrayList<String> getAssignedRules() {
		return assignedRules;
	}
	
	

}
