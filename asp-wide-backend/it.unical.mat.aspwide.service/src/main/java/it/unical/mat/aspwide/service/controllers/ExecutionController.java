package it.unical.mat.aspwide.service.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unical.mat.aspwide.service.logic.ExecutionLogic;
import it.unical.mat.aspwide.service.model.execution.ExecutableFile;
import it.unical.mat.aspwide.service.model.execution.ExecutionResult;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;

@RestController
@RequestMapping("/execution")
public class ExecutionController {
	
	private final ExecutionLogic logic;

	public ExecutionController(ExecutionLogic logic) {
		this.logic = logic;
	}
	
	@PostMapping(value = "/execute")
	public ResponseEntity<ExecutionResult> validate(@RequestBody ExecutableFile req){
		ExecutionResult resp = this.logic.executeCode(req);
		
		return ResponseEntity.ok(resp);
	}
	
	@PostMapping(value = "/execute/runconfig")
	public ResponseEntity<ExecutionResult> validate(@RequestBody RunConfiguration runconfig){
		ExecutionResult resp = this.logic.executeRunConfiguration(runconfig);
		
		return ResponseEntity.ok(resp);
	}
	
	
	@PostMapping(value = "/execute/stop")
	public void stopExecution(){
		this.logic.stopExecution();
	}
	
	
	
//	@PostMapping(value = "/executetest")
//	public ResponseEntity<ExecutionResult> executeTest(@RequestBody ExecutableFile req){
//		ExecutionResult resp = this.logic.executeTest(req);
//		
//		return ResponseEntity.ok(resp);
//	}
	
	
	
}
