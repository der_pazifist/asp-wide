/* Generated By:JJTree: Do not edit this line. AC2wA_AssertConstraintInExactly.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=AC2wA_,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package it.unical.mat.aspwide.service.validation.aspcore2.annotations;

public
class AC2wA_AssertConstraintInExactly extends SimpleNode {
  public AC2wA_AssertConstraintInExactly(int id) {
    super(id);
  }

  public AC2wA_AssertConstraintInExactly(AspCore2wAnnotationsParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(AspCore2wAnnotationsParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=ee59fd51f17019c69674715f42ccf9fd (do not edit this line) */
