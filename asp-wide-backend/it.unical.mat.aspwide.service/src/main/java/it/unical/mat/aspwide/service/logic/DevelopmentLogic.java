package it.unical.mat.aspwide.service.logic;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AC2wA_Atom;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AC2wA_Program;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AC2wA_VariableTerm;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AspCore2wAnnotationsAbstractParserVisitor;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AspCore2wAnnotationsParserVisitorImpl;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AspCore2wAnnotationsParser;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.ParseException;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.TokenMgrError;
import it.unical.mat.aspwide.service.validation.utils.ValidationUtils;
import it.unical.mat.aspwide.service.model.development.CodeError;
import it.unical.mat.aspwide.service.model.development.CodeWarning;
import it.unical.mat.aspwide.service.model.development.TestableFile;
import it.unical.mat.aspwide.service.model.development.ValidatableFile;
import it.unical.mat.aspwide.service.model.development.ValidationResult;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.aspwide.service.model.testing.TestSuite;
import it.unical.mat.aspwide.service.model.testing.TestSuiteResult;
import it.unical.mat.aspwide.service.testing.RuleCrawler;
import it.unical.mat.aspwide.service.testing.TestingEngine;

@Component
public class DevelopmentLogic {
	
	private final AspCore2wAnnotationsAbstractParserVisitor parserVisitor;
	private final TestingEngine testingEngine;

	public DevelopmentLogic(
			AspCore2wAnnotationsAbstractParserVisitor visitor,
			TestingEngine te) {
		this.parserVisitor = visitor;
		this.testingEngine = te;
	}
	
	public TestSuiteResult testRunConfiguration(RunConfiguration config) {
		String programCode = this.getProgramContentFromRunConfiguration(config);
		TestableFile anonymousProgram = new TestableFile(config.getName(), "", programCode);
		
		return testCode(anonymousProgram);
	}
	
	public TestSuiteResult testCode(TestableFile request) {
		if(request.getContent() == null)
			return null;
		
		TestSuite ts = new TestSuite();
		ts.setSolverType(request.getSolverType());
		
		// add rules (and blocks) to test suite
		RuleCrawler rc = new RuleCrawler(ts);
		rc.crawlForRules(request.getContent());
		
		try {
			AspCore2wAnnotationsParser parser = new AspCore2wAnnotationsParser(
					new ByteArrayInputStream(request.getContent().getBytes()));
			parser.configureTestSuite(ts);
			parser.program();
		}
		catch(ParseException ex) {
			ex.printStackTrace();
		}
		catch(TokenMgrError err) {
			err.printStackTrace();
		}
		
		TestSuiteResult tsr = testingEngine.executeTestSuite(ts);

		return tsr;
	}
	
	public ValidationResult validateCode(ValidatableFile request) {
		ValidationResult response = new ValidationResult(request);
		
		// check for errors
		response.setErrors(this.checkCodeValidity(request.getContent()));
		response.getErrors().addAll(this.checkSemanticErrors(request.getContent()));
		
		// check for warnings
		if(response.isExecutable())
			response.setWarnings(this.checkWarnings(request.getContent()));
		
		return response;
	}
	
	private List<CodeError> checkCodeValidity(String code) {
		List<CodeError> errors = new ArrayList<CodeError>();
		
		if(code == null || code.isEmpty()) {
			CodeError ce = new CodeError();
			ce.name = "ERROR: no code specified for validation.";
			ce.description = "ERROR: no code specified for validation.";
			errors.add(ce);
			return errors;
		}
		
		try {
			AspCore2wAnnotationsParser parser = new AspCore2wAnnotationsParser(
					new ByteArrayInputStream( code.getBytes() ));
			parser.program();
		}
		catch(ParseException ex) {
			ex.printStackTrace();

			CodeError ce = new CodeError();
			ce.line = ex.currentToken.beginLine;
			ce.startIndex = ex.currentToken.next.beginColumn;
			ce.endIndex = ex.currentToken.next.endColumn;
			
			String tmpErrMsg = ex.getMessage();
			tmpErrMsg = tmpErrMsg.replaceFirst("line [0-9]+", "line "+ce.line);
			tmpErrMsg = tmpErrMsg.replaceFirst("column [0-9]+", "column "+ce.startIndex);
			ce.name = tmpErrMsg;
			
			ce.description = ex.getStackTrace().toString();
			errors.add(ce);
		}
		catch(TokenMgrError err) {
			err.printStackTrace();
		}
		
		return errors;

	}
	
	private List<CodeError> checkSemanticErrors(String code) {
		List<CodeError> errors = new ArrayList<CodeError>();
		
		if(code == null) {
			CodeError ce = new CodeError();
			ce.name = "ERROR: no code specified for semantic error checking.";
			ce.description = "ERROR: no code specified for semantic error checking.";
			errors.add(ce);
			
			return errors;
		}

		String codeWithoutComments = this.removeComments(code);
		
		// extract the rules of the program
		List<String> rules = new ArrayList<String>();
		Pattern rulePattern = Pattern.compile(ValidationUtils.getInstance().ruleRegex);
		Matcher matcher = rulePattern.matcher(codeWithoutComments);
		while(matcher.find()) {
			rules.add(matcher.group());
		}
		
		// check the rules of the program
		int currLine = 1;
		int currPos = 0;
		for(String rule: rules) {
			
			// update rule position
			int countNewLines = rule.split("\\n").length-1;
			if(countNewLines > 0){
				currLine += countNewLines;
				currPos = 0;
			}
						
			try {
				this.parserVisitor.resetVisitor();
				AC2wA_Program program = ValidationUtils.getInstance().createParserProgram(rule);
				program.jjtAccept(this.parserVisitor, null);
				
				// check for rule safety
				for(AC2wA_VariableTerm var: ((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getNegVars()) {
					if(!((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getPosVars().contains(var)) {
						
						CodeError ce = new CodeError();
						ce.line = currLine;
						ce.startIndex = currPos;
						ce.endIndex = currPos + rule.length();
						
						ce.name = "ERROR: Rule safety violation at line "+currLine+". The variable '"+var.jjtGetValue().toString()+"' does not appear in any positive atom.";
						ce.description = "ERROR: Rule safety violation at line "+currLine+". The variable '"+var.jjtGetValue().toString()+"' does not appear in any positive atom.";
						errors.add(ce);
					}
				}
				
			}
			catch(ParseException ex) {
				ex.printStackTrace();

				// These errors are already tracked by function checkCodeValidity()
				
				CodeError ce = new CodeError();
				ce.line = currLine;
				// correct beginColumn by -2 as the parser has one symbol lookahead and is 2 position off
				ce.startIndex = currPos  + ex.currentToken.next.beginColumn-2;
				ce.endIndex = currPos  + ex.currentToken.next.endColumn;
				
				String tmpErrMsg = ex.getMessage();
				tmpErrMsg = tmpErrMsg.replaceFirst("line [0-9]+", "line "+currLine);
				tmpErrMsg = tmpErrMsg.replaceFirst("column [0-9]+", "column "+ce.startIndex);
				ce.name = tmpErrMsg;
				
				ce.description = ex.getStackTrace().toString();
				errors.add(ce);
			}
			catch(TokenMgrError err) {
				err.printStackTrace();
			}

			currPos += rule.length();
		}
		
		return errors;

	}
	
	// removes comments but keeps newlines
	private String removeComments(String code) {
		String newCode = code;
		Pattern rulePattern = Pattern.compile(ValidationUtils.getInstance().commentRegex);
		Matcher matcher = rulePattern.matcher(code);
		
		while(matcher.find()) {
			String match = matcher.group();
			String replacementString = "";
			int newlineCount = match.split("\n").length;
			
			for(int i = 0; i < newlineCount-1; i++)
				replacementString = replacementString + "\n";
			
			newCode = newCode.replace(match, replacementString);
		}
		
		return newCode;
	}
	
	@SuppressWarnings("unchecked")
	private List<CodeWarning> checkWarnings(String code) {
		List<CodeWarning> warnings = new ArrayList<CodeWarning>();

		if(code == null) {
			CodeWarning cw = new CodeWarning();
			cw.name = "WARNING: no code specified for checking warnings.";
			cw.description = "WARNING: no code specified for checking warnings.";
			warnings.add(cw);
			
			return warnings;
		}
		
		String codeWithoutComments = this.removeComments(code);
		
		// extract the rules of the program
		List<String> rules = new ArrayList<String>();
		Pattern rulePattern = Pattern.compile(ValidationUtils.getInstance().ruleRegex);
		Matcher matcher = rulePattern.matcher(codeWithoutComments);
		while(matcher.find()) {
			rules.add(matcher.group());
		}
		
		this.parserVisitor.resetVisitor();
		ArrayList<AC2wA_Atom> allAtoms = new ArrayList<AC2wA_Atom>();
		ArrayList<AC2wA_Atom> headAtoms = new ArrayList<AC2wA_Atom>();
//		ArrayList<ASPCore2Atom> bodyAtoms = new ArrayList<ASPCore2Atom>();
		
		
		// get all the atoms from visitor for later
		try {
			AC2wA_Program program = ValidationUtils.getInstance().createParserProgram(code);
			program.jjtAccept(this.parserVisitor, null);
			
			// copy for later
			allAtoms = (ArrayList<AC2wA_Atom>) ((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getAllAtoms().clone();
			headAtoms = (ArrayList<AC2wA_Atom>) ((AspCore2wAnnotationsParserVisitorImpl) this.parserVisitor).getHeadAtoms().clone();
//			bodyAtoms = (ArrayList<ASPCore2Atom>) ((ASPCore2ParserVisitorImpl) this.parserVisitor).getBodyAtoms().clone();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		// check the rules of the program
		int currLine = 1;
		for(String rule: rules) {
			
			// update rule position
			int countNewLines = rule.split("\\n").length-1;
			if(countNewLines > 0){
				currLine += countNewLines;
			}
						
			try {
				this.parserVisitor.resetVisitor();
				AC2wA_Program program = ValidationUtils.getInstance().createParserProgram(rule);
				program.jjtAccept(this.parserVisitor, null);
				
				// check for warning: all atom should appear in a head
				for(AC2wA_Atom atom: ((AspCore2wAnnotationsParserVisitorImpl)this.parserVisitor).getBodyAtoms()){
					// check if the current atom occurs in any head
					if(!headAtoms.stream().anyMatch((currAtom -> currAtom.equalsName(atom)))) {
						CodeWarning cw = new CodeWarning();
						cw.line = currLine;
						cw.name = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' should also appear in the head of a rule. Line "+currLine+".";
						cw.description = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' should also appear in the head of a rule."+currLine+".";

						warnings.add(cw);
					}
				}
				
				// check for warning: atoms with different arity
				for(AC2wA_Atom atom: ((AspCore2wAnnotationsParserVisitorImpl)this.parserVisitor).getAllAtoms()){
					// check if the current atom occurs with a different arity
					if(allAtoms.stream().anyMatch(currAtom -> (currAtom.equalsName(atom) && currAtom.getArity() != atom.getArity()))) {
						CodeWarning cw = new CodeWarning();
						cw.line = currLine;
						cw.name = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' was already defined with a different arity. Line "+currLine+".";
						cw.description = "WARNING: The atom '"+atom.getAtomName()+"/"+atom.getArity()+"' was already defined with a different arity. Line "+currLine+".";

						warnings.add(cw);
					}
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		return warnings;
	}
	
	private String getProgramContentFromRunConfiguration(RunConfiguration config) {
		StringBuilder sb = new StringBuilder();
		
		for(String filePath: config.getFiles()) {
			try {
				File f = new File(filePath);
				String content = new String(Files.readAllBytes(f.toPath()));
				sb.append(content);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return sb.toString();
	}
	
}
