package it.unical.mat.aspwide.service.validation.utils;

import java.io.ByteArrayInputStream;

import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AC2wA_Program;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.AspCore2wAnnotationsParser;
import it.unical.mat.aspwide.service.validation.aspcore2.annotations.ParseException;


public class ValidationUtils {
	
	private static ValidationUtils instance;
	private AspCore2wAnnotationsParser parser;
	public String ruleRegex = "([\\d\\w(){}+\\-*/~:,=<>#!_|?;\"\\s\\%]+)((\\.*\\s*\\[[\\d\\w+\\-*/()@,\\s]+\\]*)|(\\.)*)";
	
	// matches single line comments, multi line comments and annotations
	public String commentRegex = "(%\\*\\*?[^*]*\\*?\\*%)|(%.*)";
	
	public static ValidationUtils getInstance() {
        if (instance == null)
            instance = new ValidationUtils();
        return instance;
    }
	
	public ValidationUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public AC2wA_Program createParserProgram(String code) throws ParseException {
		if(this.parser == null)
			this.parser = new AspCore2wAnnotationsParser(new ByteArrayInputStream(code.getBytes()));
		else
			this.parser.ReInit(new ByteArrayInputStream(code.getBytes()));
		
		return this.parser.program();
	}
	
	
	
}
