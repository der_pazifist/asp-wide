package it.unical.mat.aspwide.service.files;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import it.unical.mat.aspwide.service.files.FileDao;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.aspwide.service.model.files.LogicFolder;
@Component
public class FileDaoImpl implements FileDao {
	
	@Value("${filesystemhelper.rootfolder}")
	private String ROOT_FOLDER_NAME;
	
	@Value("${filesystemhelper.workspacefolder}")
	private String WORKSPACE_FOLDER_NAME;
	
	@Value("${filesystemhelper.configurationfolder}")
	private String CONFIG_FOLDER_NAME;
	
	private FileSystemHelper fsHelper;

	public FileDaoImpl() {
		super();
	}

	@Override
	public LogicFolder getFiles() {
		try {
			this.initFsHelper();
			return this.fsHelper.getUserRootFolder();
		} catch (IOException e) {
			e.printStackTrace();
			return new LogicFolder("ERROR", "ERROR", "", new ArrayList<>(), new ArrayList<>());
		}
	}


	@Override
	public void addFile(LogicFolder folder, String uuid) {
		try {
			this.initFsHelper();
			this.fsHelper.addLogicItem(folder, uuid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateFile(LogicFolder folder, String uuid) {
		try {
			this.initFsHelper();
			this.fsHelper.updateLogicItem(folder, uuid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteFile(LogicFolder folder, String uuid) {
		try {
			this.initFsHelper();
			this.fsHelper.deleteLogicItem(folder, uuid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initFsHelper() {
		if(this.fsHelper == null)
			this.fsHelper = new FileSystemHelper(ROOT_FOLDER_NAME, WORKSPACE_FOLDER_NAME, CONFIG_FOLDER_NAME);
	}

	@Override
	public List<RunConfiguration> getRunConfigurations() {
		try {
			this.initFsHelper();
			return this.fsHelper.getRunConfigurations();
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<RunConfiguration>();
		}
	}

	@Override
	public void updateRunConfigurations(List<RunConfiguration> configs) {
		try {
			this.initFsHelper();
			this.fsHelper.updateRunConfigurations(configs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
