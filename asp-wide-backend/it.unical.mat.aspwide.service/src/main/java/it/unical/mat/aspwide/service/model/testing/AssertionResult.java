package it.unical.mat.aspwide.service.model.testing;

public class AssertionResult {
	private String name;
	private String executedCode;
	private String executionOutput;
	private boolean succeeded;
	
	public AssertionResult() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExecutedCode() {
		return executedCode;
	}

	public void setExecutedCode(String executedCode) {
		this.executedCode = executedCode;
	}

	public String getExecutionOutput() {
		return executionOutput;
	}

	public void setExecutionOutput(String executionOutput) {
		this.executionOutput = executionOutput;
	}

	public boolean isSucceeded() {
		return succeeded;
	}

	public void setSucceeded(boolean succeeded) {
		this.succeeded = succeeded;
	}

}
