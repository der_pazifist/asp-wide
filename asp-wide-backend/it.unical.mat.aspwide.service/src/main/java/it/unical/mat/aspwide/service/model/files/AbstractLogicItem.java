package it.unical.mat.aspwide.service.model.files;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public abstract class AbstractLogicItem {
	
	protected String name;
	protected String uuid;
	protected String path;
	protected String hash;

	public AbstractLogicItem() {
		super();
	}

	public AbstractLogicItem(String uuid, String name, String path) {
		super();
		this.name = name;
		this.uuid = uuid;
		this.setPath(path);
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
		
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(path.getBytes());
			this.hash = new String(messageDigest.digest());
		} catch (NoSuchAlgorithmException e) {
			this.hash = "ERROR: hash calculation failed";
			e.printStackTrace();
		}
		
	}

	public String getHash() {
		return hash;
	}
	
	

	
}
