/* Generated By:JJTree: Do not edit this line. AC2wA_AggregateElement.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=AC2wA_,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package it.unical.mat.aspwide.service.validation.aspcore2.annotations;

public
class AC2wA_AggregateElement extends SimpleNode {
  public AC2wA_AggregateElement(int id) {
    super(id);
  }

  public AC2wA_AggregateElement(AspCore2wAnnotationsParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(AspCore2wAnnotationsParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=aea3ecbf16d7108107a497526f65bb37 (do not edit this line) */
