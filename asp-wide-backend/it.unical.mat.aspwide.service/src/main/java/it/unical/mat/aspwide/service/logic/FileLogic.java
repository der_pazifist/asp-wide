package it.unical.mat.aspwide.service.logic;

import java.util.List;

import org.springframework.stereotype.Component;

import it.unical.mat.aspwide.service.files.FileDao;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.aspwide.service.model.files.LogicFolder;

@Component
public class FileLogic {
	
	private FileDao fileDao;

	public FileLogic(FileDao fileDao) {
		super();
		this.fileDao = fileDao;
	}
	
	public LogicFolder getFiles() {
		return this.fileDao.getFiles();
	}
	
	public void addFile(LogicFolder folder, String uuid) {
		this.fileDao.addFile(folder, uuid);
	}
	
	public void updateFile(LogicFolder folder, String uuid) {
		this.fileDao.updateFile(folder, uuid);
	}
	
	public void deleteFile(LogicFolder folder, String uuid) {
		this.fileDao.deleteFile(folder, uuid);
	}
	
	public List<RunConfiguration> getRunConfigurations(){
		return this.fileDao.getRunConfigurations();
	}
	
	public void updateRunConfigurations(List<RunConfiguration> configs){
		this.fileDao.updateRunConfigurations(configs);
	}

}
