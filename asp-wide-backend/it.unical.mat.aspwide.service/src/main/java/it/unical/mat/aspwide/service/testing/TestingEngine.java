package it.unical.mat.aspwide.service.testing;


import org.springframework.stereotype.Component;

import it.unical.mat.aspwide.service.logic.ExecutionLogic;
import it.unical.mat.aspwide.service.model.execution.ExecutableFile;
import it.unical.mat.aspwide.service.model.execution.ExecutionResult;
import it.unical.mat.aspwide.service.model.execution.Model;
import it.unical.mat.aspwide.service.model.execution.SolverType;
import it.unical.mat.aspwide.service.model.testing.*;

@Component
public class TestingEngine {
	
	private final String ASSERT_HELPER_ATOM = "assertCheckingHelperAtom";
	private final ExecutionLogic executionLogic;

	public TestingEngine(ExecutionLogic el) {
		this.executionLogic = el;
	}
	
	public TestSuiteResult executeTestSuite(TestSuite ts) {
		TestSuiteResult tsr = new TestSuiteResult(ts);
		
		for(Test test: ts.getTests()) {
			try {
				TestResult tr = this.executeTest(test, ts);
				tsr.addTestResult(tr);
			} catch(Exception e) {
				tsr.addTestError(
						new TestError(
								e.getMessage(),
								e.getStackTrace().toString()));
			}
		}
		

		return tsr;
	}
	
	private TestResult executeTest(Test test, TestSuite ts) throws Exception {
		TestResult testResult = new TestResult(test);
		
		for(Assertion as: test.getAssertions()) {
			AssertionResult ar = this.executeAssertion(as, test, ts);
			testResult.addAssertionResult(ar);
		}
		
		return testResult;
	}
	
	private AssertionResult executeAssertion(Assertion as, Test test, TestSuite ts) throws Exception {
		AssertionResult ar = null;
		StringBuilder intermediateProgram = new StringBuilder();
		
		// specify input
		if(test.getInput() != null) {
			intermediateProgram.append(test.getInput()+"\n");
		}
		
		// specify scope
		for(String scopeName: test.getScopes()) {
			if(ts.containsBlock(scopeName)) {
				for(String ruleName: ts.getBlock(scopeName).getAssignedRules()) {
					if(ts.containsRule(ruleName)) {
						intermediateProgram.append(ts.getRule(ruleName)+"\n");
					}
					else {
						throw new Exception("ERROR: Rule with name '"+ruleName+"' in block '"+scopeName+"' used in scope of test '"+test.getName()+"' cannot be found.");
					}
				}
			}
			else if (ts.containsRule(scopeName)) {
				intermediateProgram.append(ts.getRule(scopeName)+"\n");
			}
			else {
				throw new Exception("ERROR: Scope with name '"+scopeName+"' in test '"+test.getName()+"' cannot be found.");
			}
		}
		
		// check the right assertion
		if(as instanceof AssertTrueInAll) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertTrueInAll)as, ts.getSolverType());
		}
		else if(as instanceof AssertNoAnswerSet) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertNoAnswerSet)as, ts.getSolverType());
		}
		else if(as instanceof AssertTrueInAtLeast) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertTrueInAtLeast)as, ts.getSolverType());
		}
		else if(as instanceof AssertTrueInAtMost) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertTrueInAtMost)as, ts.getSolverType());
		}
		else if(as instanceof AssertTrueInExactly) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertTrueInExactly)as, ts.getSolverType());
		}
		else if(as instanceof AssertFalseInAll) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertFalseInAll)as, ts.getSolverType());
		}
		else if(as instanceof AssertFalseInAtLeast) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertFalseInAtLeast)as, ts.getSolverType());
		}
		else if(as instanceof AssertFalseInAtMost) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertFalseInAtMost)as, ts.getSolverType());
		}
		else if(as instanceof AssertFalseInExactly) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertFalseInExactly)as, ts.getSolverType());
		}
		else if(as instanceof AssertConstraintForAll) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertConstraintForAll)as, ts.getSolverType());
		}
		else if(as instanceof AssertConstraintInAtLeast) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertConstraintInAtLeast)as, ts.getSolverType());
		}
		else if(as instanceof AssertConstraintInAtMost) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertConstraintInAtMost)as, ts.getSolverType());
		}
		else if(as instanceof AssertConstraintInExactly) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertConstraintInExactly)as, ts.getSolverType());
		}
		else if(as instanceof AssertBestModelCost) {
			ar = checkAssertion(intermediateProgram.toString(), (AssertBestModelCost)as, ts.getSolverType());
		}

		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertTrueInAll as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		sb.append(":- ");
		for(int i = 0; i < atoms.length; i++) {
			sb.append(atoms[i].trim());
			
			if((i+1) < atoms.length)
				sb.append(", ");
		}
		sb.append(".\n");
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionTrueInAll", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile);
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == 0) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertNoAnswerSet as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		StringBuilder sb = new StringBuilder(code);
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionNoAnswerSet", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, 1);
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == 0) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertTrueInAtLeast as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		for(String atom: atoms) {
			sb.append(":- not ");
			sb.append(atom.trim());
			sb.append(".\n");
		}
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionTrueInAtLeast", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile);
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() >= as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertTrueInAtMost as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		for(String atom: atoms) {
			sb.append(":- not ");
			sb.append(atom.trim());
			sb.append(".\n");
		}
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionTrueAtMost", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, (as.getAssertCount()+1));
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() <= as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertTrueInExactly as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		for(String atom: atoms) {
			sb.append(":- not ");
			sb.append(atom.trim());
			sb.append(".\n");
		}
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionTrueInExactly", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, (as.getAssertCount()+1));
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertFalseInAll as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		sb.append(":- not ");
		for(int i = 0; i < atoms.length; i++) {
			sb.append(atoms[i].trim());
			if((i+1) < atoms.length)
				sb.append(", not ");
		}
		sb.append(".\n");
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionFalseInAll", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, 1);
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == 0) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertFalseInAtLeast as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		for(String atom: atoms) {
			sb.append(":- ");
			sb.append(atom.trim());
			sb.append(".\n");
		}
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionFalseInAtLeast", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, as.getAssertCount());
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertFalseInAtMost as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		for(String atom: atoms) {
			sb.append(":- ");
			sb.append(atom.trim());
			sb.append(".\n");
		}
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionFalseInAtMost", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, (as.getAssertCount()+1));
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() <= as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertFalseInExactly as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		String[] atoms = as.getAtoms().split("\\.");
		
		StringBuilder sb = new StringBuilder(code);
		for(String atom: atoms) {
			sb.append(":- ");
			sb.append(atom.trim());
			sb.append(".\n");
		}
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionFalseInExactly", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, (as.getAssertCount()+1));
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertConstraintForAll as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		StringBuilder sb = new StringBuilder(code);
		sb.append(this.ASSERT_HELPER_ATOM);
		sb.append(as.getConstraint());
		sb.append("\n");
		sb.append(":- not ");
		sb.append(this.ASSERT_HELPER_ATOM);
		sb.append(".\n");
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionConstraint", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, 1);
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == 0) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertConstraintInAtLeast as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		StringBuilder sb = new StringBuilder(code);
		sb.append(as.getConstraint());
		sb.append("\n");
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionConstraintInAtLeast", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, as.getAssertCount());
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() >= as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertConstraintInAtMost as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		StringBuilder sb = new StringBuilder(code);
		sb.append(as.getConstraint());
		sb.append("\n");
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionConstraintInAtMost", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, (as.getAssertCount()+1));
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() <= as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertConstraintInExactly as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		StringBuilder sb = new StringBuilder(code);
		sb.append(as.getConstraint());
		sb.append("\n");
		ar.setExecutedCode(sb.toString());
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionConstraintInExactly", ar.getExecutedCode(), "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile, (as.getAssertCount()+1));
		ar.setExecutionOutput(er.getResult());
		
		if(er.getModels() != null && er.getModels().size() == as.getAssertCount()) {
			ar.setSucceeded(true);
		}
		
		return ar;
	}
	
	private AssertionResult checkAssertion(String code, AssertBestModelCost as, SolverType st) {
		AssertionResult ar = new AssertionResult();
		ar.setName(as.getClass().getSimpleName());
		ar.setExecutedCode(code);
		
		ExecutableFile testFile = new ExecutableFile("checkAssertionBestModelCost", code, "");
		testFile.setSolverType(st);
		ExecutionResult er = this.executionLogic.executeCode(testFile);
		ar.setExecutionOutput(er.getResult());
		
		for(Model m: er.getModels()) {
			if(m.isOptimum() &&
					m.getCostAtLevel(as.getLevel()) == as.getCost()) {
				ar.setSucceeded(true);
			}
		}

		return ar;
	}
	
}
