package it.unical.mat.aspwide.service.validation.aspcore2.annotations;


public abstract class AspCore2wAnnotationsAbstractParserVisitor implements AspCore2wAnnotationsParserVisitor {

	public abstract void resetVisitor();
	
	private Object defaultVisit(SimpleNode node, Object data){
		node.childrenAccept(this, data);
        return data;
    }

	@Override
	public Object visit(SimpleNode node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Program node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Statements node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Query node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Statement node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Comment node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Head node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Body node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Disjunction node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Choice node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_ChoiceElements node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_ChoiceElement node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Aggregate node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AggregateElements node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AggregateElement node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AggregateFunction node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Optimize node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_OptimizeElements node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_OptimizeFunction node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_WeightAtLevel node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_NafLiterals node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_NafLiteral node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_ClassicalLiteral node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_BuiltinAtom node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Binop node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Terms node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Term node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Arithop node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Annotation node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AnnotationList node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AnnotationExpression node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_RuleDefinition node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_BlockDefinition node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_TestDefinition node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_ProgramDefinition node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}
	
	@Override
	public Object visit(AC2wA_ProgramFilesList node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_ProgramFile node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}
	
	@Override
	public Object visit(AC2wA_RuleReferenceList node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_RuleReference node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}
	
	@Override
	public Object visit(AC2wA_InputFilesList node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_InputFile node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_ReferenceList node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Reference node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertList node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Assertion node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertType node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}
	
	@Override
	public Object visit(AC2wA_AssertNoAnswerSet node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertTrueInAll node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertTrueInAtLeast node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertTrueInAtMost node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertTrueInExactly node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertFalseInAll node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertFalseInAtLeast node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertFalseInAtMost node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertFalseInExactly node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertConstraint node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertConstraintInExactly node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertConstraintInAtLeast node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertConstraintInAtMost node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AssertBestModelCost node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_NafAggregate node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AggregateDefinition node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_NafClassicalLiteral node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_Atom node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_NegativeBuiltinAtom node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_NegativeBinop node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_GroundTerm node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_VariableTerm node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_BasicTerms node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_BasicTerm node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_WeakTerms node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_WeakTerm node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}

	@Override
	public Object visit(AC2wA_AtomName node, Object data) {
		// TODO Auto-generated method stub
		return defaultVisit(node, data);
	}
	
	
	
}
