package it.unical.mat.aspwide.service.model.development;

public class CodeWarning {
	
	public String name;
	public String description;
	public int line;
	
	public CodeWarning() {
		// TODO Auto-generated constructor stub
	}

	public CodeWarning(String name, String description, int line) {
		super();
		this.name = name;
		this.description = description;
		this.line = line;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	
	
}
