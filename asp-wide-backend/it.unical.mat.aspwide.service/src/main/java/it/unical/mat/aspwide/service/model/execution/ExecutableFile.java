package it.unical.mat.aspwide.service.model.execution;

import it.unical.mat.aspwide.service.model.files.LogicFile;

public class ExecutableFile extends LogicFile {
	
	private SolverType solverType;
	private int solvedModels;

	public ExecutableFile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExecutableFile(String filename, String content, String path) {
		super(filename, path, content);
		// TODO Auto-generated constructor stub
	}

	public SolverType getSolverType() {
		return solverType;
	}

	public void setSolverType(SolverType solverType) {
		this.solverType = solverType;
	}

	public int getSolvedModels() {
		return solvedModels;
	}

	public void setSolvedModels(int solvedModels) {
		this.solvedModels = solvedModels;
	}
	
	
}
