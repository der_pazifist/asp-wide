package it.unical.mat.aspwide.service.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unical.mat.aspwide.service.logic.FileLogic;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.aspwide.service.model.files.LogicFolder;


@RestController
@RequestMapping("/files")
public class FilesController {
	
	private final FileLogic logic;

	public FilesController(FileLogic logic) {
		this.logic = logic;
	}
	
	@GetMapping()
	public ResponseEntity<LogicFolder> getFiles(){
		return ResponseEntity.ok(this.logic.getFiles());
	}
	
	@PostMapping(value = "/{uuid}")
	public void addFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
		this.logic.addFile(folder, uuid);
	}
	
	@PutMapping(value = "/{uuid}")
	public void updateFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
		this.logic.updateFile(folder, uuid);
	}
	
//	@DeleteMapping(value = "/{uuid}")
//	public void deleteFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
//		this.logic.deleteFile(folder, uuid);
//	}
	
	// delete request must be implemented as PUT-request since the file-tree does not exist on server-side
	@PutMapping(value = "/delete/{uuid}")
	public void deleteFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
		this.logic.deleteFile(folder, uuid);
	}
	
	@GetMapping("/configurations")
	public ResponseEntity<List<RunConfiguration>> getRunConfigurations(){
		return ResponseEntity.ok(this.logic.getRunConfigurations());
	}
	
	// updates run configurations
	@PostMapping(value = "/configurations")
	public void updateRunConfigurations(@RequestBody List<RunConfiguration> configs){
		this.logic.updateRunConfigurations(configs);
	}
}
