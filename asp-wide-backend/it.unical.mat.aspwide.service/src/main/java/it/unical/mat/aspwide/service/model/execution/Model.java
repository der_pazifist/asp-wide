package it.unical.mat.aspwide.service.model.execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Model {
	
	private ArrayList<Atom> atoms;
	private boolean isOptimum;
	private HashMap<Integer, Integer> costsAtLevel;
	
	public Model() {
		this.atoms = new ArrayList<Atom>();
		this.isOptimum = false;
		this.costsAtLevel = new HashMap<Integer, Integer>();
	}
	
	public void addAtom(Atom atom) {
		this.atoms.add(atom);
	}
	
	public void addCostAtlevel(int cost, int level) {
		this.costsAtLevel.put(level, cost);
	}

	public ArrayList<Atom> getAtoms() {
		return atoms;
	}

	public void setAtoms(ArrayList<Atom> atoms) {
		this.atoms = atoms;
	}

	public boolean isOptimum() {
		return isOptimum;
	}

	public void setOptimum(boolean isOptimum) {
		this.isOptimum = isOptimum;
	}

	public int getCostAtLevel(int level) {
		return this.costsAtLevel.get(level);
	}
	
	// recode cost levels (as clingo does not output levels they could be coded with negative numbers)
	// converts: -1, -2, -3 to 3, 2, 1
	public void recodeCostLevels() {
		if(!this.costsAtLevel.containsKey(-1))
			return; // no recoding has to be done as the levels are not negative
		
		HashMap<Integer, Integer> newCostsAtLevel = new HashMap<Integer, Integer>();
		List<Integer> keys = this.costsAtLevel.keySet().stream().sorted().collect(Collectors.toList());
		int currLevel = 1; // start with lowest level
		
		for(int i: keys) {
			
			// level conversion
			// -1 -> 3
			// -2 -> 2
			// -3 -> 1
			// this is reasonable as levels with higher number are more important
			
			newCostsAtLevel.put(currLevel, this.costsAtLevel.get(i));
			currLevel++;
		}
		
		// replace with new levels
		this.costsAtLevel = newCostsAtLevel;
	}
	
	
}
