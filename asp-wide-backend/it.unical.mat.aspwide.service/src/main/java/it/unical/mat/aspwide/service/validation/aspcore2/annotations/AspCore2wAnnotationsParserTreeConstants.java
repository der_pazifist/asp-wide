/* Generated By:JavaCC: Do not edit this line. AspCore2wAnnotationsParserTreeConstants.java Version 5.0 */
package it.unical.mat.aspwide.service.validation.aspcore2.annotations;

public interface AspCore2wAnnotationsParserTreeConstants
{
  public int JJTPROGRAM = 0;
  public int JJTSTATEMENTS = 1;
  public int JJTQUERY = 2;
  public int JJTSTATEMENT = 3;
  public int JJTCOMMENT = 4;
  public int JJTHEAD = 5;
  public int JJTBODY = 6;
  public int JJTDISJUNCTION = 7;
  public int JJTCHOICE = 8;
  public int JJTCHOICEELEMENTS = 9;
  public int JJTCHOICEELEMENT = 10;
  public int JJTNAFAGGREGATE = 11;
  public int JJTAGGREGATE = 12;
  public int JJTAGGREGATEDEFINITION = 13;
  public int JJTAGGREGATEELEMENTS = 14;
  public int JJTAGGREGATEELEMENT = 15;
  public int JJTAGGREGATEFUNCTION = 16;
  public int JJTOPTIMIZE = 17;
  public int JJTOPTIMIZEELEMENTS = 18;
  public int JJTOPTIMIZEFUNCTION = 19;
  public int JJTWEIGHTATLEVEL = 20;
  public int JJTNAFLITERALS = 21;
  public int JJTNAFLITERAL = 22;
  public int JJTNAFCLASSICALLITERAL = 23;
  public int JJTCLASSICALLITERAL = 24;
  public int JJTATOM = 25;
  public int JJTNEGATIVEBUILTINATOM = 26;
  public int JJTBUILTINATOM = 27;
  public int JJTNEGATIVEBINOP = 28;
  public int JJTBINOP = 29;
  public int JJTTERMS = 30;
  public int JJTTERM = 31;
  public int JJTGROUNDTERM = 32;
  public int JJTVARIABLETERM = 33;
  public int JJTBASICTERMS = 34;
  public int JJTBASICTERM = 35;
  public int JJTWEAKTERMS = 36;
  public int JJTWEAKTERM = 37;
  public int JJTARITHOP = 38;
  public int JJTATOMNAME = 39;
  public int JJTANNOTATION = 40;
  public int JJTANNOTATIONLIST = 41;
  public int JJTANNOTATIONEXPRESSION = 42;
  public int JJTRULEDEFINITION = 43;
  public int JJTBLOCKDEFINITION = 44;
  public int JJTTESTDEFINITION = 45;
  public int JJTPROGRAMDEFINITION = 46;
  public int JJTRULEREFERENCELIST = 47;
  public int JJTRULEREFERENCE = 48;
  public int JJTPROGRAMFILESLIST = 49;
  public int JJTPROGRAMFILE = 50;
  public int JJTINPUTFILESLIST = 51;
  public int JJTINPUTFILE = 52;
  public int JJTREFERENCELIST = 53;
  public int JJTREFERENCE = 54;
  public int JJTASSERTLIST = 55;
  public int JJTASSERTION = 56;
  public int JJTASSERTTYPE = 57;
  public int JJTASSERTNOANSWERSET = 58;
  public int JJTASSERTTRUEINALL = 59;
  public int JJTASSERTTRUEINATLEAST = 60;
  public int JJTASSERTTRUEINATMOST = 61;
  public int JJTASSERTTRUEINEXACTLY = 62;
  public int JJTASSERTFALSEINALL = 63;
  public int JJTASSERTFALSEINATLEAST = 64;
  public int JJTASSERTFALSEINATMOST = 65;
  public int JJTASSERTFALSEINEXACTLY = 66;
  public int JJTASSERTCONSTRAINT = 67;
  public int JJTASSERTCONSTRAINTINEXACTLY = 68;
  public int JJTASSERTCONSTRAINTINATLEAST = 69;
  public int JJTASSERTCONSTRAINTINATMOST = 70;
  public int JJTASSERTBESTMODELCOST = 71;


  public String[] jjtNodeName = {
    "Program",
    "Statements",
    "Query",
    "Statement",
    "Comment",
    "Head",
    "Body",
    "Disjunction",
    "Choice",
    "ChoiceElements",
    "ChoiceElement",
    "NafAggregate",
    "Aggregate",
    "AggregateDefinition",
    "AggregateElements",
    "AggregateElement",
    "AggregateFunction",
    "Optimize",
    "OptimizeElements",
    "OptimizeFunction",
    "WeightAtLevel",
    "NafLiterals",
    "NafLiteral",
    "NafClassicalLiteral",
    "ClassicalLiteral",
    "Atom",
    "NegativeBuiltinAtom",
    "BuiltinAtom",
    "NegativeBinop",
    "Binop",
    "Terms",
    "Term",
    "GroundTerm",
    "VariableTerm",
    "BasicTerms",
    "BasicTerm",
    "WeakTerms",
    "WeakTerm",
    "Arithop",
    "AtomName",
    "Annotation",
    "AnnotationList",
    "AnnotationExpression",
    "RuleDefinition",
    "BlockDefinition",
    "TestDefinition",
    "ProgramDefinition",
    "RuleReferenceList",
    "RuleReference",
    "ProgramFilesList",
    "ProgramFile",
    "InputFilesList",
    "InputFile",
    "ReferenceList",
    "Reference",
    "AssertList",
    "Assertion",
    "AssertType",
    "AssertNoAnswerSet",
    "AssertTrueInAll",
    "AssertTrueInAtLeast",
    "AssertTrueInAtMost",
    "AssertTrueInExactly",
    "AssertFalseInAll",
    "AssertFalseInAtLeast",
    "AssertFalseInAtMost",
    "AssertFalseInExactly",
    "AssertConstraint",
    "AssertConstraintInExactly",
    "AssertConstraintInAtLeast",
    "AssertConstraintInAtMost",
    "AssertBestModelCost",
  };
}
/* JavaCC - OriginalChecksum=03b9b7708a46b79a8a9a49c7dfaa6b0e (do not edit this line) */
