package it.unical.mat.aspwide.service.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.unical.mat.aspwide.service.logic.DevelopmentLogic;
import it.unical.mat.aspwide.service.model.development.TestableFile;
import it.unical.mat.aspwide.service.model.development.ValidatableFile;
import it.unical.mat.aspwide.service.model.development.ValidationResult;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.aspwide.service.model.testing.TestSuiteResult;

@RestController
@RequestMapping("/development")
public class DevelopmentController {
	
	private final DevelopmentLogic logic;

	public DevelopmentController(DevelopmentLogic logic) {
		this.logic = logic;
	}
	
	@PostMapping(value = "/validate")
	public ResponseEntity<ValidationResult> validate(@RequestBody ValidatableFile req){
		ValidationResult resp = this.logic.validateCode(req);
		
		return ResponseEntity.ok(resp);
	}
	
	@PostMapping(value = "/test")
	public ResponseEntity<TestSuiteResult> test(@RequestBody TestableFile req){
		TestSuiteResult resp = this.logic.testCode(req);
		
		return ResponseEntity.ok(resp);
	}
	
	@PostMapping(value = "/test/runconfig")
	public ResponseEntity<TestSuiteResult> testRunConfig(@RequestBody RunConfiguration config){
		TestSuiteResult resp = this.logic.testRunConfiguration(config);
		
		return ResponseEntity.ok(resp);
	}
	
	
}
