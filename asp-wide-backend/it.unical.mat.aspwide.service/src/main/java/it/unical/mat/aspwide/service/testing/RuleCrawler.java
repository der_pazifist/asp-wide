package it.unical.mat.aspwide.service.testing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.unical.mat.aspwide.service.model.testing.TestSuite;

public class RuleCrawler {
	
	public final String RULE_REGEX = "([\\d\\w(){}+\\-*/~:,=<>#!_|?;\"\\s\\%]+)((\\.*\\s*\\[[\\d\\w+\\-*/()@,\\s]+\\]*)|(\\.)*)";
	
	/*
		Full match	231-295	@rule(name="MyCool23Group_Not",  block="myblock") **%
		Group 1.	237-261	name="MyCool23Group_Not"
		Group 2.	261-279	,  block="myblock"
		Group 3.	264-279	block="myblock"
	*/
	public final String RULE_ANNOTATION_REGEX = "@rule\\(\\s*(name\\s*=\\s*\\\"[a-zA-Z0-9_]*\\\")\\s*(,\\s*(block\\s*=\\s*\\\"[a-zA-Z0-9_]*\\\")\\s*)?\\)\\s*\\*\\*%";
	

	
	private TestSuite ts;

	public RuleCrawler(TestSuite ts) {
		this.ts = ts;
	}
	
	public void crawlForRules(String code) {
		Pattern rulePattern = Pattern.compile(this.RULE_REGEX);		
		Pattern ruleAnnotationPattern = Pattern.compile(this.RULE_ANNOTATION_REGEX);
		Matcher annotationMatcher = ruleAnnotationPattern.matcher(code);
		
		while(annotationMatcher.find()) {
			// here we found @rule(name="r1", block="myblock")   \n   **%
			String nameGroup = annotationMatcher.group(1); // name = "r1"
			String blockGroup = annotationMatcher.group(3); // block = "myBlock"
			
			Matcher ruleMatcher = rulePattern.matcher(code.substring(annotationMatcher.end()));
			
			if(ruleMatcher.find()) {
				// we found a subsequent answer set rule
				String rule = ruleMatcher.group().trim();
				String ruleName = this.valueOfKVP(nameGroup);
				this.ts.addOrUpdateRule(ruleName, rule);
				
				if(blockGroup != null) {
					this.ts.addRuleAssignment(this.valueOfKVP(blockGroup), ruleName);
				}
			}
			else {
				System.out.println("ERROR: no following rule found!");
			}
			
		}
	}
	
	private String valueOfKVP(String kvp) {
		String[] parts = kvp.trim().split("=");
		return parts[1].replace("\"", "");
	}

}
