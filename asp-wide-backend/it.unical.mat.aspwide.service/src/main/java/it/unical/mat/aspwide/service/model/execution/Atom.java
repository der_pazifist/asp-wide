package it.unical.mat.aspwide.service.model.execution;

public class Atom {
	
	private String value;
	
	public Atom() {
		// TODO Auto-generated constructor stub
	}
	
	public Atom(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
