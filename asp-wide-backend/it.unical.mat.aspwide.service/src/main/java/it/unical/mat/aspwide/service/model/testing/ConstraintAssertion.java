package it.unical.mat.aspwide.service.model.testing;

public abstract class ConstraintAssertion extends Assertion {
	
	private String constraint;
	private int assertCount;
	
	public String getConstraint() {
		return constraint;
	}
	
	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}
	
	public int getAssertCount() {
		return assertCount;
	}
	
	public void setAssertCount(int assertCount) {
		this.assertCount = assertCount;
	}
	
}
