package it.unical.mat.aspwide.service.logic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import it.unical.mat.aspwide.service.model.development.CodeError;
import it.unical.mat.aspwide.service.model.development.ValidatableFile;
import it.unical.mat.aspwide.service.model.development.ValidationResult;
import it.unical.mat.aspwide.service.model.execution.Atom;
import it.unical.mat.aspwide.service.model.execution.ExecutableFile;
import it.unical.mat.aspwide.service.model.execution.ExecutionResult;
import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.wrapper.DLVInputProgram;
import it.unical.mat.wrapper.DLVInputProgramImpl;
import it.unical.mat.wrapper.DLVInvocation;
import it.unical.mat.wrapper.DLVInvocation.DLVInvocationState;
import it.unical.mat.wrapper.DLVInvocationException;
import it.unical.mat.wrapper.DLVWrapper;
import it.unical.mat.wrapper.Model;
import it.unical.mat.wrapper.ModelHandler;
import it.unical.mat.wrapper.ModelResult;
import it.unical.mat.wrapper.Predicate;
import it.unical.mat.wrapper.SolverType;

@Component
public class ExecutionLogic {
	
	@Value("${developmentservice.validate.url}")
	private String DEVELOPMENT_SERVICE_URL;
	
//	@Value("${dlv.solver.path}")
//	private String DLV_SOLVER_PATH;
	
	@Value("${dlv2.solver.path}")
	private String DLV2_SOLVER_PATH;
	
	@Value("${clingo.solver.path}")
	private String CLINGO_SOLVER_PATH;
	
	private DLVInvocation invoc;
	
	public ExecutionLogic() {
		// TODO Auto-generated constructor stub
	}
	
	public ExecutionResult executeCode(ExecutableFile request) {
		return this.executeCode(request, request.getSolvedModels());
	}
	
	public ExecutionResult executeRunConfiguration(RunConfiguration config) {
		ExecutableFile exeFile = this.getFileFromRunConfiguration(config);
		return this.executeCode(exeFile, exeFile.getSolvedModels());
	}
	
	public ExecutionResult executeCode(ExecutableFile request, int evaluationCount) {
		ExecutionResult resp = new ExecutionResult(this.checkCodeForExecution(request));
		
		if(!resp.isExecutable()) {
			return resp;
		}
		
		 resp = this.executeSolver(request.getContent(), resp, evaluationCount, request.getSolverType());
		
		return resp;
	}
	
	// execute the given code on the dlv solver
	private ExecutionResult executeSolver(String code, ExecutionResult er, int evaluationCount, it.unical.mat.aspwide.service.model.execution.SolverType solverType) {
		DLVInputProgram inputProgram = new DLVInputProgramImpl();
		inputProgram.addText(code);
		
		StringBuilder output = new StringBuilder();
		ModelHandler modelHandler = new ModelHandler() {

			@Override
			public void handleResult(DLVInvocation obsd, ModelResult res) {
				// this represents one valid model
				Model foundModel = (Model) res;
				it.unical.mat.aspwide.service.model.execution.Model model = new it.unical.mat.aspwide.service.model.execution.Model();
				
				if(foundModel.isBest())
					model.setOptimum(true);
				
				foundModel.beforeFirst();
				output.append("ANSWER\n");
				while(foundModel.hasMorePredicates()) {
					Predicate predicate = foundModel.nextPredicate();
					
					predicate.beforeFirst();
					while(predicate.hasMoreLiterals()) {
						String atomStr = predicate.nextLiteral().toString();
						
						model.addAtom(new Atom(atomStr));
						
						output.append(atomStr);
						output.append(". ");
					}
				}
				output.append("\n");
				
				// check if there are different kinds of levels (costs for e.g. weak constraints)
				if(foundModel.getLevels().size() > 0) {
					output.append("COST ");
					
					// check if solver produced negative levels (this is the case with CLINGO)
					if(foundModel.getLevels().contains(-1)) {
						int realLevel = foundModel.getLevels().size();
						
						for(int currLevel: foundModel.getLevels()) {
							int currCost = foundModel.getCost(currLevel);
							
							model.addCostAtlevel(currCost, currLevel);
							
							output.append(currCost);
							output.append("@");
							output.append(realLevel--);
							output.append(" ");
						}
					}
					else {
						for(int currLevel: foundModel.getLevels()) {
							int currCost = foundModel.getCost(currLevel);
							
							model.addCostAtlevel(currCost, currLevel);
							
							output.append(currCost);
							output.append("@");
							output.append(currLevel);
							output.append(" ");
						}
					}
					
					
					output.append("\n");
				}
				
				if(foundModel.isBest()) {
					output.append("OPTIMAL\n");
				}
//				output.append("\n");
				
				er.addModel(model);
			}
		};
		
		try {
			
			if(solverType == it.unical.mat.aspwide.service.model.execution.SolverType.CLINGO) {
				this.invoc = DLVWrapper.getInstance().createInvocation(CLINGO_SOLVER_PATH, SolverType.CLINGO);
			}
			else {
				this.invoc = DLVWrapper.getInstance().createInvocation(this.DLV2_SOLVER_PATH, true);
			}

			this.invoc.setInputProgram(inputProgram);
			this.invoc.addOption("-n " + evaluationCount);
			this.invoc.subscribe(modelHandler);
			this.invoc.run();
			this.invoc.waitUntilExecutionFinishes();
		} catch(Exception e) {
			e.printStackTrace();
			output.append("\nERROR: execution aborted due to an error.");
		}
		
		// recode cost levels (as clingo does not output levels they are coded with negative numbers)
		for(it.unical.mat.aspwide.service.model.execution.Model m: er.getModels()) {
			m.recodeCostLevels();
		}
		
		if(output.toString().isEmpty())
			er.setResult("No models found.");
		else {
			er.setResult(output.toString());
		}
		
		return er;
			
	}
	
	public void stopExecution() {
		if(invoc == null || invoc.getState() != DLVInvocationState.RUNNING)
			return;
		
		try {
			this.invoc.killDlv();
			this.invoc.reset();
		} catch (DLVInvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private ExecutableFile getFileFromRunConfiguration(RunConfiguration config) {
		StringBuilder sb = new StringBuilder();
		
		for(String filePath: config.getFiles()) {
			try {
				File f = new File(filePath);
				String content = new String(Files.readAllBytes(f.toPath()));
				sb.append(content);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		ExecutableFile execFile = new ExecutableFile(config.getName(), sb.toString(), "");
		execFile.setSolverType(config.getSolver());
		execFile.setSolvedModels(config.getSolvedModels());
		return execFile;
	}
	
	// call the development service and check for executability of the given code
	private ValidationResult checkCodeForExecution(ExecutableFile file) {
		ValidatableFile requestObj = new ValidatableFile(file.getFilename(), file.getPath(), file.getContent());
		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<ValidatableFile> request = new HttpEntity<>(requestObj);
		ResponseEntity<ValidationResult> response = restTemplate
		  .exchange(this.DEVELOPMENT_SERVICE_URL, HttpMethod.POST, request, ValidationResult.class);

		if(response.getStatusCode() == HttpStatus.OK)
			return response.getBody();
		else{
			ValidationResult res = new ValidationResult(requestObj);
			res.setErrors(Arrays.asList(
					new CodeError(
							"ERROR: no connection to development service.", 
							"ERROR: no connection to development service.",
							0)
						));
			
			return res;
		}
	}

}
