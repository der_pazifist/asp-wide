package it.unical.mat.aspwide.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AspWideBackendService {
	public static void main(String[] args) {
		SpringApplication.run(AspWideBackendService.class, args);
	}
}
