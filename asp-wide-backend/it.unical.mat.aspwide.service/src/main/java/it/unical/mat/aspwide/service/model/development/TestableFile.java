package it.unical.mat.aspwide.service.model.development;

import it.unical.mat.aspwide.service.model.execution.SolverType;
import it.unical.mat.aspwide.service.model.files.LogicFile;

public class TestableFile extends LogicFile {
	
	private SolverType solverType;

	public TestableFile() {
		// TODO Auto-generated constructor stub
	}

	public TestableFile(String name, String path, String content) {
		super(name, path, content);
		// TODO Auto-generated constructor stub
	}

	public TestableFile(String content) {
		super(content);
		// TODO Auto-generated constructor stub
	}

	public SolverType getSolverType() {
		return solverType;
	}

	public void setSolverType(SolverType solverType) {
		this.solverType = solverType;
	}

}
