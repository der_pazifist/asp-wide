package it.unical.mat.aspwide.service.files;

import java.util.List;

import it.unical.mat.aspwide.service.model.execution.RunConfiguration;
import it.unical.mat.aspwide.service.model.files.LogicFolder;

public interface FileDao {
	
	LogicFolder getFiles();
	
	void addFile(LogicFolder folder, String uuid);
	
	void updateFile(LogicFolder folder, String uuid);
	
	void deleteFile(LogicFolder folder, String uuid);
	
	List<RunConfiguration> getRunConfigurations();
	
	void updateRunConfigurations(List<RunConfiguration> configs);
}
