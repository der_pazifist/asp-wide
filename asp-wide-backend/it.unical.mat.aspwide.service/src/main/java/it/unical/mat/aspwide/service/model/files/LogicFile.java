package it.unical.mat.aspwide.service.model.files;

public class LogicFile extends AbstractLogicItem {
	
	protected String content;

	public LogicFile() {
		super();
	}

	public LogicFile(String name, String path, String content) {
		super();
		this.name = name;
		this.path = path;
		this.content = content;
	}

	public LogicFile(String content) {
		super();
		this.content = content;
	}

	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getFilename() {
		return name;
	}
	
	public void setFilename(String filename) {
		this.name = filename;
	}
}
