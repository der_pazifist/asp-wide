package it.unical.mat.aspwide.service.model.execution;

import java.util.ArrayList;

import it.unical.mat.aspwide.service.model.development.ValidatableFile;
import it.unical.mat.aspwide.service.model.development.ValidationResult;

public class ExecutionResult extends ValidationResult {
	
	protected ArrayList<Model> models;
	protected String result;

	public ExecutionResult() {
		super();
		this.models = new ArrayList<Model>();
	}

	public ExecutionResult(String result) {
		super();
		this.result = result;
		this.models = new ArrayList<Model>();
	}

	public ExecutionResult(String filename, String path, String content) {
		super(filename, path, content);
		this.models = new ArrayList<Model>();
	}

	public ExecutionResult(ValidatableFile file) {
		super(file);
		this.models = new ArrayList<Model>();
	}
	
	public ExecutionResult(ValidationResult result) {
		super(result);
		this.models = new ArrayList<Model>();
	}
	
	public void addModel(Model m) {
		this.models.add(m);
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public ArrayList<Model> getModels() {
		return models;
	}

	public void setModels(ArrayList<Model> models) {
		this.models = models;
	}
}
