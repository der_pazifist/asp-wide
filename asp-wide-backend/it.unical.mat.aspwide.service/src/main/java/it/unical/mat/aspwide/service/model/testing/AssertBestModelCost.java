package it.unical.mat.aspwide.service.model.testing;

public class AssertBestModelCost extends Assertion {
	
	private int cost;
	private int level;

	public AssertBestModelCost() {
		// TODO Auto-generated constructor stub
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	

}
