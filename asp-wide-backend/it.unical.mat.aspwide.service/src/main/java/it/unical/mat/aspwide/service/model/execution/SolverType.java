package it.unical.mat.aspwide.service.model.execution;

public enum SolverType {
	DLV2,
	CLINGO
}
