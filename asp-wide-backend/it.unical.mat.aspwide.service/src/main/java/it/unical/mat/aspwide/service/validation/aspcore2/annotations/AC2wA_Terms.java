/* Generated By:JJTree: Do not edit this line. AC2wA_Terms.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=AC2wA_,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package it.unical.mat.aspwide.service.validation.aspcore2.annotations;


public
class AC2wA_Terms extends SimpleNode {
  public AC2wA_Terms(int id) {
    super(id);
  }

  public AC2wA_Terms(AspCore2wAnnotationsParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(AspCore2wAnnotationsParserVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
  
  public void acceptTermsForCountingArity(AspCore2wAnnotationsParserVisitor visitor, AC2wA_Atom atom) {
      for (int i = 0; i < children.length; i++)
          if (children[i] instanceof AC2wA_Term) {
              ((AC2wA_Term) children[i]).acceptTermForCountingArity(visitor, atom);
          } else if (children[i] instanceof AC2wA_Terms)
              ((AC2wA_Terms) children[i]).acceptTermsForCountingArity(visitor, atom);
  }
}
/* JavaCC - OriginalChecksum=271bd15e8db290b27aa877170a952bff (do not edit this line) */
