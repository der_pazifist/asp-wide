package it.unical.mat.aspwide.service.model.execution;

import java.io.Serializable;
import java.util.ArrayList;

public class RunConfiguration implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -601964087072606194L;
	
	protected String name;
	protected String options;
	protected SolverType solver;
	protected int solvedModels;
	protected ArrayList<String> files;

	public RunConfiguration() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public ArrayList<String> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<String> files) {
		this.files = files;
	}

	public SolverType getSolver() {
		return solver;
	}

	public void setSolver(SolverType solver) {
		this.solver = solver;
	}

	public int getSolvedModels() {
		return solvedModels;
	}

	public void setSolvedModels(int solvedModels) {
		this.solvedModels = solvedModels;
	}

	
}
