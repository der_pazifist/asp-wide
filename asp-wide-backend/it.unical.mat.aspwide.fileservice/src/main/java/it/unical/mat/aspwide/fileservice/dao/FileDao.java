package it.unical.mat.aspwide.fileservice.dao;

import it.unical.mat.aspwide.model.LogicFolder;

public interface FileDao {
	
	LogicFolder getFiles();
	
	void addFile(LogicFolder folder, String uuid);
	
	void updateFile(LogicFolder folder, String uuid);
	
	void deleteFile(LogicFolder folder, String uuid);
}
