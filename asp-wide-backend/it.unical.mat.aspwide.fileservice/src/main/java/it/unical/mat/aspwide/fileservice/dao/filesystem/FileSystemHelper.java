package it.unical.mat.aspwide.fileservice.dao.filesystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.FileUtils;

import it.unical.mat.aspwide.model.AbstractLogicItem;
import it.unical.mat.aspwide.model.LogicFile;
import it.unical.mat.aspwide.model.LogicFolder;

public class FileSystemHelper {

	private String rootFolderName;

	public FileSystemHelper(String rootFolderName) {
		super();
		this.rootFolderName = rootFolderName;
	}

	private String getBaseFolderPath() {
		return System.getProperty("user.home") + File.separator + "Documents" + File.separator + rootFolderName;
	}
	
	private String getBaseFolderParentPath() {
		return System.getProperty("user.home") + File.separator + "Documents";
	}

	public LogicFolder getUserRootFolder() throws IOException {
		String path = getBaseFolderPath();
		File customDir = new File(path);
		
		if (customDir.exists()) {
		    return iterateFolder(customDir);
		} else if (customDir.mkdirs()) {
			return iterateFolder(customDir);
		} 
		
	    throw new IOException("ERROR: Could not create asp-wide workspace folder.");
	}
	
	private LogicFolder iterateFolder(File file) {
		LogicFolder currentRoot = new LogicFolder(
				UUID.randomUUID().toString(), 
				file.getName(), 
				new ArrayList<LogicFolder>(), 
				new ArrayList<LogicFile>());
		
		File[] fList = file.listFiles();
		
		if(fList != null) {
			for(File f: fList) {
				if(f.isDirectory()) {
//					List<LogicFolder> folders = currentRoot.getFolders();
//					folders.add(iterateFolder(f));
//					currentRoot.setFolders(folders);
					currentRoot.getFolders().add(iterateFolder(f));
				}
				else{
					LogicFile logicFile = new LogicFile();
					logicFile.setUuid(UUID.randomUUID().toString());
					logicFile.setFilename(f.getName());
					try {
						logicFile.setContent(new String(Files.readAllBytes(f.toPath())));
					} catch (IOException e) {
						e.printStackTrace();
						logicFile.setContent("ERROR: Loading file contents failed.");
					}
					currentRoot.getFiles().add(logicFile);
				}
			}
		}
		
		return currentRoot;
	}

	public void addLogicItem(LogicFolder rootFolder, String uuid) throws IOException {
		createLogicItemRecursive(rootFolder, uuid, new ArrayList<String>());
	}
	
	private void createLogicItemRecursive(AbstractLogicItem currItem, String uuid, List<String> parts) throws IOException {
		// found new file?
		if(currItem.getUuid().equals(uuid)) {
			parts.add(currItem.getName());
			String basePath = this.getBaseFolderParentPath();
			String relativePath = parts.stream().collect(Collectors.joining(File.separator));
			String fullPath = basePath + File.separator + relativePath;
			File newItem = new File(fullPath);

			// create if folder
			if(currItem instanceof LogicFolder) {
				if(!newItem.exists()){
					if(!newItem.mkdirs()) {
						throw new IOException("ERROR: Could not create new filesystem item.");
					}
				}
			}
			// create if file
			else {
				if(!newItem.exists()){
					if(!newItem.createNewFile()) {
						throw new IOException("ERROR: Could not create new filesystem item.");
					}
				}
				else {
					throw new IOException("ERROR: Filesystem item already exists.");
				}
			}
			
			return;
		}

		// do the recursive calls if this is a folder
		if(currItem instanceof LogicFolder) {
			parts.add(currItem.getName());
			
			// recursive calls for all files
			for(LogicFile f: ((LogicFolder) currItem).getFiles()) {
				createLogicItemRecursive(f, uuid, new ArrayList<String>(parts));
			}
			
			// recursive calls for all folders
			for(LogicFolder f: ((LogicFolder) currItem).getFolders()) {
				createLogicItemRecursive(f, uuid, new ArrayList<String>(parts));
			}
		}
	}

	public void updateLogicItem(LogicFolder root, String uuid) throws IOException {
		setLogicFileContentRecursive(root, uuid, new ArrayList<String>());
	}
	
	private void setLogicFileContentRecursive(AbstractLogicItem currItem, String uuid, List<String> parts) throws IOException {
		// found file?
		if(currItem.getUuid().equals(uuid)) {
			parts.add(currItem.getName());
			String basePath = getBaseFolderParentPath();
			String relativePath = parts.stream().collect(Collectors.joining(File.separator));
			String fullPath = basePath + File.separator + relativePath;
			File item = new File(fullPath);

			if(currItem instanceof LogicFolder) {
				throw new FileNotFoundException("ERROR: Could not set content as target item is not a file.");

			}
			else {
				// check if file exists first
				if(item.exists()){
					Files.write(item.toPath(), Arrays.asList(((LogicFile)currItem).getContent()), Charset.forName("UTF-8"), StandardOpenOption.TRUNCATE_EXISTING);
				}
				else {
					throw new FileNotFoundException("ERROR: Could not set content as file does not exist");
				}
			}
			
			return;
		}

		// do the recursive calls if this is a folder
		if(currItem instanceof LogicFolder) {
			parts.add(currItem.getName());
			
			// recursive calls for all files
			for(LogicFile f: ((LogicFolder) currItem).getFiles()) {
				setLogicFileContentRecursive(f, uuid, new ArrayList<String>(parts));
			}
			
			// recursive calls for all folders
			for(LogicFolder f: ((LogicFolder) currItem).getFolders()) {
				setLogicFileContentRecursive(f, uuid, new ArrayList<String>(parts));
			}
		}
	}

	public void deleteLogicItem(LogicFolder folder, String uuid) throws IOException {
		deleteLogicItemRecursive(folder, uuid, new ArrayList<String>());
	}
	
	private void deleteLogicItemRecursive(AbstractLogicItem currItem, String uuid, List<String> parts) throws IOException {
		// found file?
		if(currItem.getUuid().equals(uuid)) {
			parts.add(currItem.getName());
			String basePath = getBaseFolderParentPath();
			String relativePath = parts.stream().collect(Collectors.joining(File.separator));
			String fullPath = basePath + File.separator + relativePath;
			File item = new File(fullPath);
			
			if(item.exists()) {
				if(currItem instanceof LogicFile) {
					if(!item.delete()) {
						throw new IOException("ERROR: Could not delete logic item.");
					}
				}
				else {
					FileUtils.deleteDirectory(item);
				}
			}
			return;
		}

		// do the recursive calls if this is a folder
		if(currItem instanceof LogicFolder) {
			parts.add(currItem.getName());
			
			// recursive calls for all files
			for(LogicFile f: ((LogicFolder) currItem).getFiles()) {
				deleteLogicItemRecursive(f, uuid, new ArrayList<String>(parts));
			}
			
			// recursive calls for all folders
			for(LogicFolder f: ((LogicFolder) currItem).getFolders()) {
				deleteLogicItemRecursive(f, uuid, new ArrayList<String>(parts));
			}
		}
	}
	
	
//	private static String findRelativePath(AbstractLogicItem currItem, String uuid, List<String> parts) {
//	if(currItem.getUuid().equals(uuid)) {
//		parts.add(currItem.getName());
//		return parts.stream().collect(Collectors.joining(File.separator));
//	}
//	String relativePath = null;
//	
//	if(currItem instanceof LogicFolder) {
//		parts.add(currItem.getName());
//		
//		// check if any containing file is the searched one
//		for(LogicFile f: ((LogicFolder) currItem).getFiles()) {
//			if(f.getUuid().equals(uuid)) {
//				parts.add(f.getName());
//				return parts.stream().collect(Collectors.joining(File.separator));
//			}
//		}
//		
//		// call function for all containing folders recursively
//		for(LogicFolder f: ((LogicFolder) currItem).getFolders()) {
//			List<String> newParts = new ArrayList<String>(parts);
//			newParts.add(f.getName());
//			String tmpRelPath = findRelativePath(f, uuid, newParts);
//			if(tmpRelPath != null)
//				relativePath = tmpRelPath;
//		}
//	}
//	
//	return relativePath;
//}
}
