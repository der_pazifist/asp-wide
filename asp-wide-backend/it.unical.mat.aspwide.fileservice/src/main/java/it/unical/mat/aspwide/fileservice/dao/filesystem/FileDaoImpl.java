package it.unical.mat.aspwide.fileservice.dao.filesystem;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import it.unical.mat.aspwide.fileservice.dao.FileDao;
import it.unical.mat.aspwide.model.LogicFolder;

@Component
public class FileDaoImpl implements FileDao {
	
	@Value("${filesystemhelper.rootfolder}")
	private String ROOT_FOLDER_NAME;
	
	private FileSystemHelper fsHelper;

	public FileDaoImpl() {
		super();
	}

	@Override
	public LogicFolder getFiles() {
		try {
			this.initFsHelper();
			return this.fsHelper.getUserRootFolder();
		} catch (IOException e) {
			e.printStackTrace();
			return new LogicFolder("ERROR", "ERROR", new ArrayList<>(), new ArrayList<>());
		}
	}


	@Override
	public void addFile(LogicFolder folder, String uuid) {
		try {
			this.initFsHelper();
			this.fsHelper.addLogicItem(folder, uuid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateFile(LogicFolder folder, String uuid) {
		try {
			this.initFsHelper();
			this.fsHelper.updateLogicItem(folder, uuid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteFile(LogicFolder folder, String uuid) {
		try {
			this.initFsHelper();
			this.fsHelper.deleteLogicItem(folder, uuid);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initFsHelper() {
		if(this.fsHelper == null)
			this.fsHelper = new FileSystemHelper(ROOT_FOLDER_NAME);
	}

}
