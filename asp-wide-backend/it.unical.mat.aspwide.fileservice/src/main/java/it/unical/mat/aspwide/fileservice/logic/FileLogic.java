package it.unical.mat.aspwide.fileservice.logic;

import org.springframework.stereotype.Component;

import it.unical.mat.aspwide.fileservice.dao.FileDao;
import it.unical.mat.aspwide.model.LogicFolder;

@Component
public class FileLogic {
	
	private FileDao fileDao;

	public FileLogic(FileDao fileDao) {
		super();
		this.fileDao = fileDao;
	}
	
	public LogicFolder getFiles() {
		return this.fileDao.getFiles();
	}
	
	public void addFile(LogicFolder folder, String uuid) {
		this.fileDao.addFile(folder, uuid);
	}
	
	public void updateFile(LogicFolder folder, String uuid) {
		this.fileDao.updateFile(folder, uuid);
	}
	
	public void deleteFile(LogicFolder folder, String uuid) {
		this.fileDao.deleteFile(folder, uuid);
	}

}
