package it.unical.mat.aspwide.fileservice.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.unical.mat.aspwide.fileservice.logic.FileLogic;
import it.unical.mat.aspwide.model.LogicFolder;


@RestController
public class FileController {
	
	private final FileLogic logic;

	public FileController(FileLogic logic) {
		this.logic = logic;
	}
	
	@GetMapping()
	public ResponseEntity<LogicFolder> getFiles(){
		return ResponseEntity.ok(this.logic.getFiles());
	}
	
	@PostMapping(value = "/{uuid}")
	public void addFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
		this.logic.addFile(folder, uuid);
	}
	
	@PutMapping(value = "/{uuid}")
	public void updateFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
		this.logic.updateFile(folder, uuid);
	}
	
//	@DeleteMapping(value = "/{uuid}")
//	public void deleteFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
//		this.logic.deleteFile(folder, uuid);
//	}
	
	// delete request must be implemented as PUT-request since the file-tree does not exist on server-side
	@PutMapping(value = "/delete/{uuid}")
	public void deleteFile(@PathVariable String uuid, @RequestBody LogicFolder folder){
		this.logic.deleteFile(folder, uuid);
	}
}
