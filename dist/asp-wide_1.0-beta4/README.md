# ASP-WIDE
ASP-WIDE is a web-based development environment for answer set programming. The environment can be run locally on your machine.

### Requirements:
  - Web browser of your choice (ideally Chrome)
  - Installed JRE or JDK (with 'java' in your PATH or environment variable)
  - Installed ASP systems CLINGO or DVL2 (ideally both)
  - Optional: local admin privileges

### How to RUN:
  - Make sure the requirements are satisfied
  - Unzip ASP-WIDE (if not already done)
  - Modify locations/paths of the ASP systems in the file `asp-wide-service.properties` (description provided)
  - Start ASP-WIDE by executing the start scripts (`start-asp-wide.bat` for Windows or `start-asp-wide.sh` for Linux)
  - Browse 'http://localhost:8094' in your web browser
  - Create and execute files written in ASP-Core-2 input language format
  - See examples provided in the ASP-WIDE folder 