import { IdeComponent } from './ide/ide.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    { path: '',   redirectTo: '/ide', pathMatch: 'full' },
    { 
      path: 'ide', 
      component: IdeComponent,
    }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { 
        enableTracing: false, // <-- debugging purposes only
        useHash: true 
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
