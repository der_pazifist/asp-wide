import { AddFolderDialogComponent } from './ide/add-folder-dialog/add-folder-dialog.component';
import { AddFileDialogComponent } from './ide/add-file-dialog/add-file-dialog.component';
import { SharingService } from './services/sharing/sharing.service';
import { RestClientService } from './services/rest-client/rest-client.service';
import { AppRoutingModule } from './app-routing.module';
import { MaterialComponentsModule } from './material-components.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MonacoEditorModule, NgxMonacoEditorConfig } from 'ngx-monaco-editor';

import { AppComponent } from './app.component';
import { IdeComponent } from './ide/ide.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { aspCore2Lang } from './utils/monarch/asp-core-2.0.lang';
import { GuidService } from './utils/guid/guid.service';
import { RunConfigDialogComponent } from './ide/run-config-dialog/run-config-dialog.component';
import { environment } from 'src/environments/environment';

export function myMonacoOnLoad(){
  (<any>window).monaco.languages.register({ id: 'aspCore2Lang' });

  (<any>window).monaco.languages.setMonarchTokensProvider('aspCore2Lang', aspCore2Lang);
}

const monacoConfig: NgxMonacoEditorConfig = {
  // baseUrl: environment.monacoBaseUrl, // configure base path for monaco editor
  defaultOptions: { scrollBeyondLastLine: false }, // pass default options to be used
  onMonacoLoad: myMonacoOnLoad // here monaco object will be available as window.monaco use this function to extend monaco editor functionality.
};

@NgModule({
  declarations: [
    AppComponent,
    IdeComponent,
    AddFileDialogComponent,
    AddFolderDialogComponent,
    RunConfigDialogComponent
  ],
  imports: [
    BrowserModule,
    MaterialComponentsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MonacoEditorModule.forRoot(monacoConfig),
    FormsModule,
    HttpClientModule
  ],
  providers: [
    RestClientService,
    SharingService,
    GuidService
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    AddFileDialogComponent,
    AddFolderDialogComponent,
    RunConfigDialogComponent
  ]
})
export class AppModule { }
