import { IExecutableFile } from './../../models/rest/executable-file.model';
import { IValidatableFile } from './../../models/rest/validatable-file.model';
import { IValidationResult } from './../../models/rest/validation-result.model';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IExecutionResult } from 'src/app/models/rest/execution-result.model';
import { ILogicFolder } from 'src/app/models/rest/logic-folder.model';
import { ITestableFile } from 'src/app/models/rest/testable-file.model';
import { ITestResult } from 'src/app/models/rest/test-result.model';
import { ITestSuiteResult } from 'src/app/models/rest/test-suite-result.model';
import { IRunConfiguration } from 'src/app/models/rest/run-configuration.model';

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  private baseUrl: string;

  private validateUrl = '/development/validate';
  private testUrl = '/development/test';
  private testRunConfigUrl = '/development/test/runconfig';
  private executeUrl = '/execution/execute';
  private stopExecutionUrl = '/execution/execute/stop';
  private executeUrl_runConfiguration = '/execution/execute/runconfig';
  private filesystemUrl_getFiles = '/files';
  private filesystemUrl_postNewLogicItem = '/files';
  private filesystemUrl_putLogicItem = '/files';
  private filesystemUrl_deleteLogicItem = '/files/delete';
  private filesystemUrl_getRunConfigurations = '/files/configurations';
  private filesystemUrl_postRunConfigurations = '/files/configurations';

  private httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = environment.restBaseUrl;
  }

  public postValidate(file: IValidatableFile) : Observable<IValidationResult> {
    return this.http.post<IValidationResult>(this.baseUrl+this.validateUrl, file, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postValidate'))
      );
  }

  public postTest(file: ITestableFile) : Observable<ITestSuiteResult> {
    return this.http.post<ITestableFile>(this.baseUrl+this.testUrl, file, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postTest'))
      );
  }

  public postTestRunConfiguration(config: IRunConfiguration) : Observable<ITestSuiteResult> {
    return this.http.post<ITestSuiteResult>(this.baseUrl+this.testRunConfigUrl, config, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postTestRunConfiguration'))
      );
  }

  public postExecute(file: IExecutableFile) : Observable<IExecutionResult> {
    return this.http.post<IExecutionResult>(this.baseUrl+this.executeUrl, file, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postExecute'))
      );
  }

  public postStopExecute() : Observable<any> {
    return this.http.post<any>(this.baseUrl+this.stopExecutionUrl, null, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postStopExecute'))
      );
  }

  public postExecuteRunConfiguration(config: IRunConfiguration) : Observable<IExecutionResult> {
    return this.http.post<IExecutionResult>(this.baseUrl+this.executeUrl_runConfiguration, config, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postExecuteRunConfiguration'))
      );
  }

  public getFiles() : Observable<ILogicFolder> {
    return this.http.get<ILogicFolder>(this.baseUrl+this.filesystemUrl_getFiles, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('getFiles'))
      );
  }

  public postNewLogicItem(workspaceRoot: ILogicFolder, newItemGuid: string) : Observable<any> {
    return this.http.post(this.baseUrl+this.filesystemUrl_postNewLogicItem+'/'+newItemGuid, workspaceRoot, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postNewLogicItem'))
      );
  }

  public putLogicItem(workspaceRoot: ILogicFolder, itemGuid: string) : Observable<any> {
    return this.http.put(this.baseUrl+this.filesystemUrl_putLogicItem+'/'+itemGuid, workspaceRoot, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('putLogicItem'))
      );
  }

  public deleteLogicItem(workspaceRoot: ILogicFolder, deleteItemGuid: string) : Observable<any> {
    return this.http.put(this.baseUrl+this.filesystemUrl_deleteLogicItem+'/'+deleteItemGuid, workspaceRoot, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('putDeleteLogicItem'))
      );
  }

  public getRunConfigurations() : Observable<Array<IRunConfiguration>> {
    return this.http.get<Array<IRunConfiguration>>(this.baseUrl+this.filesystemUrl_getRunConfigurations, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('getRunConfigurations'))
      );
  }

  public postRunConfigurations(configs: Array<IRunConfiguration>) : Observable<any> {
    return this.http.post(this.baseUrl+this.filesystemUrl_postRunConfigurations, configs, this.httpOptions)
      .pipe(
        catchError(this.handleError<any>('postRunConfigurations'))
      );
  }



  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
