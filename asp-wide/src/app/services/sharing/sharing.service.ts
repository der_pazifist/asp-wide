import { Injectable, EventEmitter } from '@angular/core';
import { IRunConfiguration } from 'src/app/models/rest/run-configuration.model';

@Injectable({
  providedIn: 'root'
})
export class SharingService {

  // event emitter for event_onClickSolve in app.component
  public solveEvent: EventEmitter<number> = new EventEmitter<number>();

  // event emitter for event_onClickTest in app.component
  public testEvent: EventEmitter<number> = new EventEmitter<number>();

  // event emitter for event_onClickMenuButton in app.component
  public fileMenuEvent: EventEmitter<any> = new EventEmitter<any>();

  // event emitter for event_onClickAddFile in app.component
  public addFileInRootEvent: EventEmitter<number> = new EventEmitter<number>();

  // event emitter for event_onClickAddFolder in app.component
  public addFolderInRootEvent: EventEmitter<number> = new EventEmitter<number>();

  // event emitter for event_onClickOpenRunConfigurations in app.component
  public openRunConfigurationsEvent: EventEmitter<number> = new EventEmitter<number>();

  private _runConfigurations: Array<IRunConfiguration> = [];

  private _selectedRunConfiguration: IRunConfiguration;

  private _selectedSolver: string = "CLINGO";

  private _solveModels: number = 1;

  constructor() { }

  public fireSolveEvent() {
    this.solveEvent.emit(0);
  }

  public fireTestEvent() {
    this.testEvent.emit(0);
  }

  public fireMenuButtonClickEvent(event: any) {
    this.fileMenuEvent.emit(event);
  }

  public fireAddFileClickEvent() {
    this.addFileInRootEvent.emit(0);
  }

  public fireAddFolderClickEvent() {
    this.addFolderInRootEvent.emit(0);
  }

  public fireOpenRunConfigurationsClickEvent() {
    this.openRunConfigurationsEvent.emit(0);
  }

  get runConfigurations(): Array<IRunConfiguration> {
    return this._runConfigurations;
  }

  set runConfigurations(config: Array<IRunConfiguration>) {
    this._runConfigurations = config;
  }

  get selectedRunConfigruation(): IRunConfiguration {
    return this._selectedRunConfiguration;
  }

  set selectedRunConfigruation(config: IRunConfiguration) {
    this._selectedRunConfiguration = config;
  }

  get selectedSolver(): string {
    return this._selectedSolver
  }
  
  set selectedSolver(solverType: string) {
    if(solverType == "CLINGO" || solverType == "DLV2")
      this._selectedSolver = solverType;
  }

  get solveModels(): number {
    return this._solveModels
  }
  
  set solveModels(modNumber: number) {
    this._solveModels = modNumber;
  }
}
