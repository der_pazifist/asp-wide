import { SharingService } from './services/sharing/sharing.service';
import { Component, OnInit } from '@angular/core';
import { aspCore2Lang } from './utils/monarch/asp-core-2.0.lang';
import { IRunConfiguration } from './models/rest/run-configuration.model';
import { MatSelectChange } from '@angular/material';
import { RestClientService } from './services/rest-client/rest-client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'asp-wide';
  
  constructor(
    public sharing: SharingService,
    private rest: RestClientService
  ){

  }

  ngOnInit(){

  }

  public event_onClickSolve(){
     this.sharing.fireSolveEvent();
  }

  public event_onClickTest(){
    this.sharing.fireTestEvent();
 }

 public event_onClickMenuButton(event){
    this.sharing.fireMenuButtonClickEvent(event);
 }

 public event_onClickAddFile(){
   this.sharing.fireAddFileClickEvent();
 }

 public event_onClickAddFolder(){
  this.sharing.fireAddFolderClickEvent();
}

  public event_onClickSolveOptions(event: any){
    event.stopPropagation();
    event.preventDefault();
  }

  public event_onClickSelectRunConfiguration(config: IRunConfiguration){
    this.sharing.selectedRunConfigruation = config;
  }

  public event_onChangeRunConfiguration(config: MatSelectChange){
    if(config.value == "openRunConfig"){
      this.sharing.fireOpenRunConfigurationsClickEvent();
    }
    else{
      this.sharing.selectedRunConfigruation = this.sharing.runConfigurations.find(rc => rc.name.localeCompare(config.value) == 0);
    }
  }

  public event_onClickOpenRunConfigurations() {
    this.sharing.fireOpenRunConfigurationsClickEvent();
  }

  public event_onClickSetSolver($event, solver: string){
    this.sharing.selectedSolver = solver;
  }

  public event_onClickStopExecution(event){
    this.rest.postStopExecute()
      .subscribe(
        resp => console.log(resp)
      );
  }


}
