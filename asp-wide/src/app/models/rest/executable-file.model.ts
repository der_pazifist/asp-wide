import { ILogicFile } from "./logic-file.model";

export interface IExecutableFile extends ILogicFile {
  solverType: string,
  solvedModels: number
}