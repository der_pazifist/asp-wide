import { ILogicFile } from "./logic-file.model";

export interface ITestableFile extends ILogicFile{
  solverType: string
}