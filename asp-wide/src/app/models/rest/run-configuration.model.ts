export interface IRunConfiguration {
  name: string,
  options: string,
  solver: string,
  solvedModels: number,
  files: Array<string>
}