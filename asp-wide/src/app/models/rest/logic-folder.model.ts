import { IAbstractLogicItem } from './abstract-logic-item.model';
import { ILogicFile } from './logic-file.model';

export interface ILogicFolder extends IAbstractLogicItem{
  files: Array<ILogicFile>,
  folders: Array<ILogicFolder>
}

