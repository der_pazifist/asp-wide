export interface ICodeError{
  name: string,
  description: string,
  line: number,
  startIndex: number,
  endIndex: number
}