import { ITest } from "./test.model";
import { IAssertionResult } from "./assertion-result.model";

export interface ITestResult extends ITest {
  assertionResults: Array<IAssertionResult>
}