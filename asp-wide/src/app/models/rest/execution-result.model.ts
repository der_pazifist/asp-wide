import { IValidationResult } from "./validation-result.model";

export interface IExecutionResult extends IValidationResult{
  result: string
}