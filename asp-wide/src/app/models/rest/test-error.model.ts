export interface ITestError {
  name: string,
  description: string
}