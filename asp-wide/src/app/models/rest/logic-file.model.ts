import { IAbstractLogicItem } from './abstract-logic-item.model';

export interface ILogicFile extends IAbstractLogicItem{
  content: string
}