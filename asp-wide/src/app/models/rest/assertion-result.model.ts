export interface IAssertionResult{
  name: string,
  executedCode: string,
  executedOutput: string,
  succeeded: boolean
}