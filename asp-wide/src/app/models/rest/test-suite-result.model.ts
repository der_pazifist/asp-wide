import { ITest } from "./test.model";
import { ITestResult } from "./test-result.model";
import { ITestError } from "./test-error.model";

export interface ITestSuiteResult{
  blocks: any,
  rules: any,
  tests: Array<ITest>,
  testResults: Array<ITestResult>,
  testErrors: Array<ITestError>
}