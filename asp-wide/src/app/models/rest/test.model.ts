export interface ITest{
  name: string,
  input: string,
  inputFile: string,
  scopes: Array<string>,
  assertions: any
}