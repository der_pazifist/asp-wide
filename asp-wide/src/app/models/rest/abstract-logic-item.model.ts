export interface IAbstractLogicItem{
  name: string,
  path: string,
  uuid: string,
  hash: string
}