import { IValidatableFile } from './validatable-file.model';
import { ICodeError } from './code-error.model';
import { ICodeWarning } from './code-warning.model';

export interface IValidationResult extends IValidatableFile{
  errors: Array<ICodeError>,
  warnings: Array<ICodeWarning>
}