export interface ICodeWarning{
  name: string,
  description: string,
  line: number,
  startIndex: number,
  endIndex: number
}