import { AddFolderDialogComponent } from './add-folder-dialog/add-folder-dialog.component';
import { GuidService } from './../utils/guid/guid.service';
import { AddFileDialogComponent } from './add-file-dialog/add-file-dialog.component';
import { ILogicFile } from './../models/rest/logic-file.model';
import { IAbstractLogicItem } from './../models/rest/abstract-logic-item.model';
import { ILogicFolder } from './../models/rest/logic-folder.model';
import { IExecutionResult } from './../models/rest/execution-result.model';
import { IExecutableFile } from './../models/rest/executable-file.model';
import { IValidationResult } from './../models/rest/validation-result.model';
import { ICodeError } from './../models/rest/code-error.model';
import { IValidatableFile } from './../models/rest/validatable-file.model';
import { RestClientService } from './../services/rest-client/rest-client.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SharingService } from '../services/sharing/sharing.service';
// import * as M from 'ngx-monaco-editor';
import { NgxEditorModel } from 'ngx-monaco-editor';
import { strictEqual } from 'assert';
import { MatTreeNestedDataSource, MatMenuTrigger, MatDialog } from '@angular/material';
import { NestedTreeControl } from '@angular/cdk/tree';
import { aspCore2Lang } from '../utils/monarch/asp-core-2.0.lang';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ITestableFile } from '../models/rest/testable-file.model';
import { ITestResult } from '../models/rest/test-result.model';
import { ITestSuiteResult } from '../models/rest/test-suite-result.model';
import { IAssertionResult } from '../models/rest/assertion-result.model';
import { RunConfigDialogComponent } from './run-config-dialog/run-config-dialog.component';
import { IRunConfiguration } from '../models/rest/run-configuration.model';


@Component({
  selector: 'app-ide',
  templateUrl: './ide.component.html',
  styleUrls: ['./ide.component.css']
})
export class IdeComponent implements OnInit {

  @ViewChild('contextMenuFileTrigger', {read: MatMenuTrigger}) contextMenuFile: MatMenuTrigger;
  @ViewChild('contextMenuFolderTrigger', {read: MatMenuTrigger}) contextMenuFolder: MatMenuTrigger;
  @ViewChild('contextMenuWorkspaceTrigger', {read: MatMenuTrigger}) contextMenuWorkspace: MatMenuTrigger;

  public editorOptions = { theme: 'vs-dark', language: 'aspCore2Lang', automaticLayout: true };
  public code: string = '% Facts \n  vtx(1). vtx(2). vtx(3). vtx(4). vtx(5). \n  edge(1, 2). edge(2, 3). edge(3, 4). edge(4, 5). edge(3, 2). edge(1, 3). edge(2, 4). edge(5, 1).\n  start(1).\n  inPath(X, Y) | outPath(X, Y) :- edge(X, Y).\n  % Auxiliary rules\n  reached(X) :- inPath(Y, X), reached(Y).\n  reached(X) :- start(X).\n  % Checking part: specify constraints on solution.\n  % All vertexes must be in the path.\n  :- vtx(X), not reached(X).\n  % A Target Node of the path cannot be the start node.\n  :- start(X), inPath(_, X).\n  % Each vertex in the path must have\n  % at most one incoming and one outgoing edge.\n  :- vtx(X), 2 <= #count{Y : inPath(X, Y)}. \n   :- vtx(X), 2 <= #count{Y : inPath(Y, X)}.';
  public editor: any;
  public editorModels: Array<monaco.editor.ITextModel> = [];
  public markers: Array<monaco.editor.IMarker> = []; 
  public output: Array<string> = [];

  // ui components
  public fileDrawerWidth: number = 300;
  public outputDrawerHeight: number = 200;
  private _drawerId: number = 0; // 0: none, 1: left drawer, 2: output
  private _mouseDown: boolean = false;
  private _mouseDownPixel: number = 0;

  // file tree members
  public workspaceRoot: ILogicFolder;
  public treeDataSource: MatTreeNestedDataSource<IAbstractLogicItem>;
  public fileTreeControl: NestedTreeControl<IAbstractLogicItem>;
  private _selectedItems: Array<IAbstractLogicItem> = [];

  // test tree members
  public testResults: ITestSuiteResult;
  public testTreeDataSource: MatTreeNestedDataSource<any>;
  public testTreeControl: NestedTreeControl<any>;

  // tab-group members
  public tabItems: Array<ILogicFile> = [];
  public selectedTabIndex: FormControl = new FormControl(-1); 
  public selectedResultTabsIndex: FormControl = new FormControl(0);

  // stores the content-update request (for interruption)
  public contentUpdateRequest: Subscription = null;
  private timer: any;

  public isExecuting: boolean = false;

  constructor(
    private rest: RestClientService,
    private sharing: SharingService,
    private dialog: MatDialog,
    private guidService: GuidService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.sharing.solveEvent
      .subscribe(
        num => {
          console.log(num);
          this.event_onRecieveSolveEvent();
      });

    this.sharing.testEvent
      .subscribe(
        num => {
          console.log(num);
          this.event_onRecieveTestEvent();
      });

    this.sharing.addFileInRootEvent
      .subscribe(
        num => {
          console.log(num);
          this.event_onClickAddFile(this.workspaceRoot);
        }
      );

    this.sharing.addFolderInRootEvent
      .subscribe(
        num => {
          console.log(num);
          this.event_onClickAddFolder(this.workspaceRoot);
        }
      );

    this.sharing.openRunConfigurationsEvent
      .subscribe(
        num => {
          console.log(num);
          this.event_onClickOpenRunConfigurations(null);
        }
      );

    this.fileTreeControl = new NestedTreeControl<IAbstractLogicItem>(this._getChildren);
    this.testTreeControl = new NestedTreeControl<any>(this._getChildrenForTestTree);
    this._loadWorkspace();
    this._loadRunConfigurations();
  }

  private _loadWorkspace(){
    // load files
    this.rest.getFiles()
      .subscribe(
        resp => {
          this.workspaceRoot = resp;
          this.initWorkspaceTree();
        },
        err => {
          console.log(err);
        }
      );
  }

  private _loadRunConfigurations(){
    this.rest.getRunConfigurations()
      .subscribe(
        resp => {
          this.sharing.runConfigurations = resp;
        },
        err => {
          console.log(err);
        }
      );
  }

  public initWorkspaceTree(){
    // init tree
    this.fileTreeControl = new NestedTreeControl<IAbstractLogicItem>(this._getChildren);
    this.treeDataSource = new MatTreeNestedDataSource<IAbstractLogicItem>();
    this.treeDataSource.data = this._getChildren(<IAbstractLogicItem>this.workspaceRoot);
  }

  private _initTestResultsTree(){
    this.testTreeControl = new NestedTreeControl<any>(this._getChildrenForTestTree);
    this.testTreeDataSource = new MatTreeNestedDataSource<any>();
    this.testTreeDataSource.data = this._getChildrenForTestTree(this.testResults);
  }

  public event_onMouseDownDragger(event: any, drawerId: number){
    console.log(event);
    this._mouseDown = true;
    if(drawerId == 1){
      this._mouseDownPixel = event.pageX;
    }
    else{
      this._mouseDownPixel = event.pageY;
    }
    
    this._drawerId = drawerId;
    // console.log(this._drawerId);
  }

  public event_onMouseUpDragger(event: any){
    // console.log('up');
    // console.log(event);
    this._mouseDown = false;
    this._drawerId = 0;
  }

  public event_onMouseMoveDragger(event: any){
    if(this._drawerId == 1 && this._mouseDown && event.pageX < 450 && event.pageX > 100){
      this.fileDrawerWidth = this.fileDrawerWidth + (event.pageX - this._mouseDownPixel);
      this._mouseDownPixel = event.pageX;
      
      // console.log('draggedX');
      // console.log(this.fileDrawerWidth);
    }
    else if(this._drawerId == 2 && this._mouseDown && event.pageY > 100){
      this.outputDrawerHeight = this.outputDrawerHeight + (this._mouseDownPixel - event.pageY);
      this._mouseDownPixel = event.pageY;
      // console.log('draggedY');
      // console.log(this.outputDrawerHeight);
    }
    // console.log('wasindmove');
  }

  public onInit(editor: any){
    this.editorModels.push(editor.model);
    this.editor = (<any>window).monaco.editor;    
  }

  private _getChildren(logicItem: IAbstractLogicItem): IAbstractLogicItem[] {
    if(('folders' in logicItem) && ('files' in logicItem)){
      let children: Array<IAbstractLogicItem> = Object.assign([], (<ILogicFolder>logicItem).folders);
      let files: Array<IAbstractLogicItem> = Object.assign([], (<ILogicFolder>logicItem).files);
      children.push(...files);
      return children;
    }
    else
      return [];
  }

  private _getChildrenForTestTree(currentNode: any) : any[] {
    if(('testResults' in currentNode)){
      let children: Array<ITestResult> = Object.assign([], (<ITestSuiteResult>currentNode).testResults);
      return children;
    }
    else if(('assertionResults' in currentNode)){
      let children: Array<IAssertionResult> = Object.assign([], (<ITestResult>currentNode).assertionResults);
      return children;
    }
    else
      return [];
  }

  public hasChildItems(idx: number, item: IAbstractLogicItem): boolean {
    if(('folders' in item) && ('files' in item)){
      return (((<ILogicFolder>item).folders != null) && ((<ILogicFolder>item).folders.length != 0)) ||
        (((<ILogicFolder>item).files != null) && ((<ILogicFolder>item).files.length != 0));
    }
    else
      return false;
  }

  public isFolder(idx: number, item: IAbstractLogicItem): boolean {
    if(('folders' in item) && ('files' in item)){
     return true;
    }
    else
      return false;
  }

  public isTestResult(idx: number, item: any): boolean {
    if(('assertionResults' in item)){
     return true;
    }
    else
      return false;
  }

  public isAssertionResult(idx: number, item: any): boolean {
    if(('succeeded' in item)){
     return true;
    }
    else
      return false;
  }

  // need to pass context as param, as this gets called from external event and has not the context of the component
  public event_onRecieveSolveEvent(){
    // check if a run configuration is selected
    if(this.sharing.selectedRunConfigruation != null){
      this._executeRunConfiguration(this.sharing.selectedRunConfigruation);
    }
    else{
      if(this.selectedTabIndex.value < 0)
      return;

      // show tests-tab on results tab group
      this.selectedResultTabsIndex.setValue(0);

      let selectedIndex: number = this.selectedTabIndex.value;
      let selectedFile: ILogicFile = this.tabItems[selectedIndex]
      let file: IExecutableFile = {
        name: selectedFile.name,
        solverType: this.sharing.selectedSolver,
        solvedModels: this.sharing.solveModels,
        path: selectedFile.path,
        content: selectedFile.content,
        uuid: selectedFile.uuid,
        hash: selectedFile.hash
      };

      this.isExecuting = true;
      this.rest.postExecute(file)
        .subscribe(result => {
          this.isExecuting = false;
          this._handleExecutionResult(result, selectedIndex)
        });
    }
  }

  private _validateCurrentProgram(){
    if(this.selectedTabIndex.value < 0)
      return;

    let selectedIndex: number = this.selectedTabIndex.value;
    let selectedFile: ILogicFile = this.tabItems[selectedIndex]
    let file: IValidatableFile = {
      name: selectedFile.name,
      path: selectedFile.path,
      content: selectedFile.content,
      uuid: selectedFile.uuid,
      hash: selectedFile.hash
    };

    this.isExecuting = true;
    this.rest.postValidate(file)
      .subscribe(
        result =>{
          this.isExecuting = false;
          this._handleValidationResult(result, selectedIndex);
      });
    
  }

  public event_onRecieveTestEvent(){
    // check if a run configuration is selected
    if(this.sharing.selectedRunConfigruation != null){
      this._testRunConfiguration(this.sharing.selectedRunConfigruation);

      // show tests-tab on results tab group
      this.selectedResultTabsIndex.setValue(1);
    }
    else{
      if(this.selectedTabIndex.value < 0)
        return;

      // show tests-tab on results tab group
      this.selectedResultTabsIndex.setValue(1);

      let selectedIndex: number = this.selectedTabIndex.value;
      let selectedFile: ILogicFile = this.tabItems[selectedIndex]
      let file: ITestableFile = {
        name: selectedFile.name,
        solverType: this.sharing.selectedSolver,
        path: selectedFile.path,
        content: selectedFile.content,
        uuid: selectedFile.uuid,
        hash: selectedFile.hash
      };

      this.isExecuting = true;
      this.rest.postTest(file)
        .subscribe(result => {
          this.isExecuting = false;
          this._handleTestResult(result)
        });
    }

  }

  public event_onClickOpenFile(node: ILogicFile){
    let idx = this.tabItems.indexOf(node);

    if(idx < 0){
      this.tabItems.push(node);
      idx = this.tabItems.indexOf(node);
    }

    this.selectedTabIndex.setValue(idx);

    setTimeout(() => {
      this._validateCurrentProgram();
    }, 500);
    
  }

  public event_onClickCloseTab(item: ILogicFile){
    let idx = this.tabItems.indexOf(item);

    this.tabItems.splice(idx, 1);
    this.editorModels.splice(idx, 1);

    if(this.tabItems.length <= 0)
      this.selectedTabIndex.setValue(-1);
  }

  public event_onChangeSelectedTab(event){
    this.selectedTabIndex.setValue(event);
  }

  public contextMenuFilePosition = { x: '0px', y: '0px' };
  public contextMenuFolderPosition = { x: '0px', y: '0px' };
  public contextMenuWorkspacePosition = { x: '0px', y: '0px' };

  public event_onClickContextMenuFile(event: MouseEvent, item: ILogicFile) {
    event.preventDefault();
    this.contextMenuFilePosition.x = event.clientX + 'px';
    this.contextMenuFilePosition.y = event.clientY + 'px';
    console.log(event);
    console.log(this.contextMenuFilePosition);
    this.contextMenuFile.menuData = { 'item': item };
    if(!this.contextMenuWorkspace.menuOpen)
      this.contextMenuFile.openMenu();
  }

  public event_onClickContextMenuFolder(event: MouseEvent, item: ILogicFile) {
    event.preventDefault();
    this.contextMenuFolderPosition.x = event.clientX + 'px';
    this.contextMenuFolderPosition.y = event.clientY + 'px';
    this.contextMenuFolder.menuData = { 'item': item };
    if(!this.contextMenuWorkspace.menuOpen)
      this.contextMenuFolder.openMenu();
  }

  public event_onClickContextMenuWorkspace(event: MouseEvent, logicIt: IAbstractLogicItem) {
    event.preventDefault();
    this.contextMenuWorkspacePosition.x = event.clientX + 'px';
    this.contextMenuWorkspacePosition.y = event.clientY + 'px';
    // this.contextMenuWorkspace.menuData = { 'logicItem': logicIt };
    if(!this.contextMenuFile.menuOpen && !this.contextMenuFolder.menuOpen)
      this.contextMenuWorkspace.openMenu();
  }

  public event_onClickAddFile(item: ILogicFolder){
    const dialogRef = this.dialog.open(AddFileDialogComponent, {
      width: '400px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      let name: string = result;

      if(name){
        this._createNewFile(this.workspaceRoot, item, name);
      }
    });
  }

  private _createNewFile(currRoot: ILogicFolder, item: ILogicFolder, newName: string){
    if(currRoot.uuid == item.uuid){
      let newFile: ILogicFile = <ILogicFile>{
        name: newName,
        content: '',
        uuid: this.guidService.newGuid()
      }

      currRoot.files.push(newFile);

      this.rest.postNewLogicItem(this.workspaceRoot, newFile.uuid)
        .subscribe(
          resp => {
            this._loadWorkspace();
          },
          err => {
            console.log(err);
          }
        );
    }

    for(let i = 0; i < currRoot.folders.length; i++){
      this._createNewFile(currRoot.folders[i], item, newName);
    }
  }

  public event_onClickAddFolder(item: ILogicFolder){
    const dialogRef = this.dialog.open(AddFolderDialogComponent, {
      width: '400px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      let name: string = result;

      if(name){
        this._createNewFolder(this.workspaceRoot, item, name);
      }
    });
  }

  private _createNewFolder(currRoot: ILogicFolder, item: ILogicFolder, newName: string){
    if(currRoot.uuid == item.uuid){
      let newFolder: ILogicFolder = <ILogicFolder>{
        name: newName,
        path: '',
        hash: '',
        content: '',
        uuid: this.guidService.newGuid(),
        files: new Array<ILogicFile>(),
        folders: new Array<ILogicFolder>()
      };

      currRoot.folders.push(newFolder);

      this.rest.postNewLogicItem(this.workspaceRoot, newFolder.uuid)
        .subscribe(
          resp => {
            this._loadWorkspace();
          },
          err => {
            console.log(err);
          }
        );
    }

    for(let i = 0; i < currRoot.folders.length; i++){
      this._createNewFolder(currRoot.folders[i], item, newName);
    }
  }

  public event_onClickDeleteLogicItem(logicItem: IAbstractLogicItem){
    this.rest.deleteLogicItem(this.workspaceRoot, logicItem.uuid)
      .subscribe(
        resp => {
          this._loadWorkspace();
        },
        err => {
          console.log(err);
        }
      );
  }

  onContextMenuAction1(item) {
    alert(`Click on Action 1 for ${item.name}`);
  }

  onContextMenuAction2(item) {
    alert(`Click on Action 2 for ${item.name}`);
  }
  

  private _handleExecutionResult(result: IExecutionResult, selectedTabIndex: number){
    // clear old output
    this.output = [];
    this.markers = [];
    if(this.editor)
      this.editor.setModelMarkers(this.editorModels[selectedTabIndex], 'aspWideIde', this.markers);

    // display warnings
    if(result.warnings && result.warnings.length > 0){
      for(let warn of result.warnings){
        this.output.push(warn.name);

        // monaco.editor.IMarker
        let newMarker: any = {
          startColumn: 0,
          endColumn: 100,
          startLineNumber: warn.line,
          endLineNumber: warn.line,
          message: warn.name,
          severity: monaco.MarkerSeverity.Warning
        }

        this.markers.push(newMarker);
      }

      // update editor markers
      if(this.editor)
        this.editor.setModelMarkers(this.editorModels[selectedTabIndex], 'aspWideIde', this.markers);
    }

    // display errors
    if(result.errors && result.errors.length > 0){
      for(let err of result.errors){
        this.output.push(err.name);

        // monaco.editor.IMarker
        let newMarker: any = {
          startColumn: err.startIndex,
          endColumn: err.endIndex,
          startLineNumber: err.line,
          endLineNumber: err.line,
          message: err.name,
          severity: monaco.MarkerSeverity.Error
        }

        this.markers.push(newMarker);
      }

      // update editor markers
      if(this.editor)
        this.editor.setModelMarkers(this.editorModels[selectedTabIndex], 'aspWideIde', this.markers);
    }
    else{
      if(result.result != null)
        this.output.push(result.result);
      else
        this.output = [];
    }

    this.changeDetectorRef.detectChanges();
  }


  private _handleValidationResult(result: IValidationResult, selectedTabIndex: number){
    // clear old output
    this.output = [];
    this.markers = [];
    this.editor.setModelMarkers(this.editorModels[selectedTabIndex], 'aspWideIde', this.markers);

    // display warnings
    if(result.warnings && result.warnings.length > 0){
      for(let warn of result.warnings){
        this.output.push(warn.name);

        // monaco.editor.IMarker
        let newMarker: any = {
          startColumn: 0,
          endColumn: 100,
          startLineNumber: warn.line,
          endLineNumber: warn.line,
          message: warn.name,
          severity: monaco.MarkerSeverity.Warning
        }

        this.markers.push(newMarker);
      }

      // update editor markers
      this.editor.setModelMarkers(this.editorModels[selectedTabIndex], 'aspWideIde', this.markers);
    }

    // display errors
    if(result.errors && result.errors.length > 0){
      for(let err of result.errors){
        this.output.push(err.name);

        // monaco.editor.IMarker
        let newMarker: any = {
          startColumn: err.startIndex,
          endColumn: err.endIndex,
          startLineNumber: err.line,
          endLineNumber: err.line,
          message: err.name,
          severity: monaco.MarkerSeverity.Error
        }

        this.markers.push(newMarker);
      }

      // update editor markers
      this.editor.setModelMarkers(this.editorModels[selectedTabIndex], 'aspWideIde', this.markers);
    }

    this.changeDetectorRef.detectChanges();
  }

  private _handleTestResult(result: ITestSuiteResult){
    console.log(result);
    this.testResults = result;
    this._initTestResultsTree();
  }

  private execute(){
    // TODO
  }

  private _instanceOfILogicFolder(logicItem: any): boolean {
    return ('folders' in logicItem) && ('files' in logicItem);
  }
  
  public event_onContentModified(event: any){
    if(this.timer)
      clearTimeout(this.timer);
    let self = this;
    this.timer = setTimeout(this.updateContentRequestCallback, 1000, self);
  }

  private updateContentRequestCallback(self){
    let modFile: ILogicFile = self.tabItems[self.selectedTabIndex.value];

    if(self.contentUpdateRequest != null){
      self.contentUpdateRequest.unsubscribe();
      self.contentUpdateRequest = null; 
    }

    self.contentUpdateRequest = self.rest.putLogicItem(self.workspaceRoot, modFile.uuid)
      .subscribe(
        resp => {
          //this._loadWorkspace();

          self._validateCurrentProgram();
          self.contentUpdateRequest = null;
          
        },
        err => {
          console.log(err);
          self.contentUpdateRequest = null;
        }
      );
  }

  public allTestsSucceeded(testResult: ITestResult) : boolean {
    if(testResult != null && testResult.assertionResults != null){
      for(let assertionResult of testResult.assertionResults){
        if(!assertionResult.succeeded)
          return false;
      }
    }
    return true;
  }

  public isTreeItemSelected(item: IAbstractLogicItem){
    return this._selectedItems.indexOf(item) >= 0;
  }

  public event_onClickTreeItem(item: IAbstractLogicItem){
    if(this._selectedItems.indexOf(item) >= 0){
      this._selectedItems.splice(this._selectedItems.indexOf(item), 1);
    }
    else{
      this._selectedItems.push(item);
    }
  }

  public event_onClickOpenRunConfigurations(item: IAbstractLogicItem){
    const dialogRef = this.dialog.open(RunConfigDialogComponent, {
      width: '800px',
      data: {
        rcs: this.sharing.runConfigurations,
        treeSI: this._selectedItems
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result == null)
        return;

      let executeConfig = result['executeRunConfiguration'];

      if(executeConfig != null){
        this.sharing.selectedRunConfigruation = executeConfig;
        this._executeRunConfiguration(executeConfig);
      }
    });
  }

  public event_onClickRunSelectedFilesDirectly(item: IAbstractLogicItem){

    if(this._selectedItems == null || this._selectedItems.length <= 0)
      return;

    let tmpRunConfig: IRunConfiguration = <IRunConfiguration>{
      name: 'anonymousRunConfiguration',
      options: '',
      solver: this.sharing.selectedSolver,
      solvedModels: this.sharing.solveModels,
      files: this._selectedItems.map(item => item.path)
    }

    this._executeRunConfiguration(tmpRunConfig);
  }

  private _executeRunConfiguration(config: IRunConfiguration){
    this.isExecuting = true;

    this.rest.postExecuteRunConfiguration(config)
      .subscribe(
        resp => {
          
          // #############################
          // HANDLE ERRORS AND WARNINGS
          // Errors and warnings after run-config execution do not
          // correspond with an actual file. Therefore no suitable
          // visualization is possible at the moment.
          // #############################

          this.isExecuting = false;
          this._handleExecutionResult(resp, this.selectedTabIndex.value);
          this.changeDetectorRef.detectChanges();
        },
        err => {
          this.isExecuting = false;
          console.log(err);
        }
      );
  }

  private _testRunConfiguration(config: IRunConfiguration){
    this.isExecuting = true;
    this.rest.postTestRunConfiguration(config)
      .subscribe(
        resp => {
          this.isExecuting = false;
          let self = this;
          this._handleTestResult(resp);
          // this.changeDetectorRef.detectChanges();
        },
        err => {
          this.isExecuting = true;
          console.log(err);
        }
      );
  }

}
