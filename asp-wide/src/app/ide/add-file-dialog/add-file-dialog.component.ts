import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-file-dialog',
  templateUrl: './add-file-dialog.component.html',
  styleUrls: ['./add-file-dialog.component.css']
})
export class AddFileDialogComponent{
  
  public filename: string = '';

  constructor(
    public dialogRef: MatDialogRef<AddFileDialogComponent>,
    // @Inject(MAT_DIALOG_DATA) public data: DialogDa
    )
  {  }

  public event_onClickCancel(){
    this.dialogRef.close();
  }

  public event_onClickCreate() {
    this.dialogRef.close(this.filename);
  }
}
