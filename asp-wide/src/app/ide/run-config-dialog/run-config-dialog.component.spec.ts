import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunConfigDialogComponent } from './run-config-dialog.component';

describe('RunConfigDialogComponent', () => {
  let component: RunConfigDialogComponent;
  let fixture: ComponentFixture<RunConfigDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunConfigDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunConfigDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
