import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatTreeNestedDataSource } from '@angular/material';
import { ILogicFolder } from 'src/app/models/rest/logic-folder.model';
import { NestedTreeControl } from '@angular/cdk/tree';
import { IAbstractLogicItem } from 'src/app/models/rest/abstract-logic-item.model';
import { IRunConfiguration } from 'src/app/models/rest/run-configuration.model';
import { RestClientService } from 'src/app/services/rest-client/rest-client.service';

@Component({
  selector: 'app-run-config-dialog',
  templateUrl: './run-config-dialog.component.html',
  styleUrls: ['./run-config-dialog.component.css']
})
export class RunConfigDialogComponent implements OnInit {

  public selectedItems: Array<IAbstractLogicItem> = [];
  public runConfigurations: Array<IRunConfiguration> = [];
  public selectedSolver: string = 'clingo';
  public configName: string = '';

  private _isNewConfiguration: boolean = true;
  public selectedRunConfiguration: IRunConfiguration;

  constructor(
    public dialogRef: MatDialogRef<RunConfigDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _rest: RestClientService
  ) {

    this.selectedItems = data['treeSI'];
    this.runConfigurations = data['rcs'];

    // if this is set, then this is a new run configuration
    if(this.selectedItems != null && this.selectedItems.length > 0){
      this.selectedRunConfiguration = <IRunConfiguration>{
        name: 'MyRunConfig'+(this.runConfigurations.length+1),
        options: '',
        solver: 'CLINGO',
        solvedModels: 1,
        files: this.selectedItems.map(item => item.path)
      };
    }
    else {
      this._clearFormData();
    }
  }

  private _clearFormData(){
    this._isNewConfiguration = false;
    this.selectedRunConfiguration = <IRunConfiguration>{
      name: '',
      options: '',
      solver: '',
      solvedModels: 1,
      files: []
    };
  }

  ngOnInit() {
  }

  public event_onClickCancel(): void {
    this.dialogRef.close();
  }

  public event_onClickDelete(){
    if(this._isNewConfiguration || this.selectedRunConfiguration.name === ''){
      return;
    }

    this.runConfigurations.splice(this.runConfigurations.indexOf(this.selectedRunConfiguration), 1);

    this._rest.postRunConfigurations(this.runConfigurations)
      .subscribe(
        res => this._clearFormData()
      );
  }

  public event_onClickSave(): void {
    if(this._isNewConfiguration)
      this.runConfigurations.push(this.selectedRunConfiguration);

    this._rest.postRunConfigurations(this.runConfigurations)
      .subscribe(
        res => this._clearFormData()
      );

    // this.dialogRef.close();
  }

  public event_onClickRun(): void {
    if(this.selectedRunConfiguration.name !== ''){
      this.dialogRef.close({
        executeRunConfiguration: this.selectedRunConfiguration
      });
    }
    else{
      this.dialogRef.close();
    }
    
  }

  public isTreeItemSelected(item: IAbstractLogicItem){
    return this.selectedItems.indexOf(item) >= 0;
  }

  public event_onClickTreeItem(item: IAbstractLogicItem){
    if(this.selectedItems.includes(item)){
      this.selectedItems.splice(this.selectedItems.indexOf(item), 1);
    }
    else{
      this.selectedItems.push(item);
    }
  }

  public isFolder(idx: number, item: IAbstractLogicItem): boolean {
    if(('folders' in item) && ('files' in item)){
     return true;
    }
    else
      return false;
  }

  public event_onClickRunConfiguration(item: IRunConfiguration){
    this._isNewConfiguration = false;
    this.selectedRunConfiguration = this.runConfigurations.find(config => config.name == item.name);

    this.selectedItems = item.files.map(
      filepath => {
        let parts: string[];

        if(filepath.indexOf('/') >= 0){
          parts = filepath.split('/');
        }
        else{
          parts = filepath.split('\\');
        }
        
        let logItem: IAbstractLogicItem = <IAbstractLogicItem>{
          name: parts[parts.length-1]
        }
        return logItem;
      }
    );
  }

  public isSelectedRunConfiguration(item: IRunConfiguration){
    return this.selectedRunConfiguration.name == item.name;
  }

  public isRunConfigurationInvalid(): boolean {
    return this.selectedRunConfiguration.name === '' || this._isNewConfiguration;
  }

}
