import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-folder-dialog',
  templateUrl: './add-folder-dialog.component.html',
  styleUrls: ['./add-folder-dialog.component.css']
})
export class AddFolderDialogComponent{
  
  public foldername: string = '';

  constructor(
    public dialogRef: MatDialogRef<AddFolderDialogComponent>,
    // @Inject(MAT_DIALOG_DATA) public data: DialogDa
    )
  {  }

  public event_onClickCancel(){
    this.dialogRef.close();
  }

  public event_onClickCreate() {
    this.dialogRef.close(this.foldername);
  }
}
