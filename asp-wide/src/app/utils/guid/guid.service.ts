import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';


@Injectable()
export class GuidService {

  constructor() { }

  public newGuid(): string {
    return UUID.UUID();
  }
}
