export const environment = {
  production: false,
  restBaseUrl: 'http://localhost:8094',
  monacoBaseUrl: 'assets'
};